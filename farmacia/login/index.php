
<!DOCTYPE html>
<html lang="es">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 


	 
    <meta http-equiv="X-UA-Compatible" content="IE=edge">     
	<title>Inicio de Sesion</title>
	<meta name="description" content="Sistema exclusivo para farmacias" />
    <meta name="author" content="Bill alanya Onofre - Desarrollador " />
    <meta name="viewport" content="width=device-width, maximum-scale=1, minimum-scale=1" />
	<link rel="stylesheet" href="../assets/plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/login.css"> 

	<!-- sweet alert -->
    <link rel="stylesheet" href="../assets/plugins/sweet-alert/css/sweet-alert.css">
    <!-- Feature detection -->
    <script src="../assets/js/jquery-1.10.2.min.js"></script>
</head>
<body class="cuerpo">
	
	<section class="bg-container">
	<div class="logos text-center">
		<div class="container ">
			<div class="col-md-4 col-sm-4 logos2"><img src="images/logos1.png" alt="" class="img-responsive"></div>
			<div class="col-md-4 col-sm-4 logos2"><br><img src="images/logos2.png" alt="" class="img-responsive"></div>
			<div class="col-md-4 col-sm-4 logos2"><img src="images/logos3.png" alt="" class="img-responsive"></div>
		</div>
	</div>
		<div class="bg-opacity">
			<div class="container ">			
				 <img src="images/imagen-pyg.png" alt="" class="img-responsive" style="margin: 10px auto;">
				<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
					<div class="login-form">
						<div class="formulario-login">
							<form action="seguridad-login.php" method='post' id="bb">
								<div class="form-group formulario-inputs">
									<!-- <label for="txtNomusu" class="etiquet">Usuario *</label> -->
									<input type="text" name="txtNomusu" id="txtNomusu"  placeholder="Usuario" class="form-control controles inputs cajas" >
								</div>				
								<div class="form-group formulario-inputs">
									<!-- <label for="txtClausu" class="etiquet">Contrase単a *</label> -->
									<input type="password" name="txtClausu" id="txtClausu" class="form-control controles inputs cajas" data-rule-required="true" data-rule-minlength="6" placeholder="Contraseña*" required="">
									<!--<small style="color: #fff; text-align: right;">¿Olvidaste tu contraseña?</small>-->
								</div>
								
								<div class="form-group text-center">
									<button type="submit" name="btnLogin" id="btn-login" class="btn-login">
										Entrar
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</section>
 
	

 <script src="../assets/plugins/sweet-alert/js/sweet-alert.min.js"></script>	
 <script src="js/valida-login.js"></script>
</body>
</html>