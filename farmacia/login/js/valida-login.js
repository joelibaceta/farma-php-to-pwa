var expr1 = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
var expr2 = /^[0-9]+$/;
$("#btn-login").click(function() {
    var Nomusu      = $("#txtNomusu").val();
    var Clausu   = $("#txtClausu").val();
    // validamos
    if (Nomusu == "" && Clausu == "") {
        $("input#txtNomusu").focus();
        sweetAlert("Oops...", "Ingrese el usuario y contraseña", "warning");
        $(".inputs").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    } else if (Nomusu == "" ) {
        $(".inputs").css({
            "border": "",
        });
        $("input#txtNomusu").focus();
        sweetAlert("Oops...", "Ingrese el usuario o correo", "warning");
        $("#txtNomusu").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    } else if (Clausu == "") {
        $(".inputs").css({
            "border": "",
        });
        $("input#txtClausu").focus();
        sweetAlert("Oops...", "Ingrese la contraseña", "warning");
        $("#txtClausu").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    }else {
        form.submit();
    }
});