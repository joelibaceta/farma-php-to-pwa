<?php
class detalleProducto{
	private $pdo;
    
   	 public $iddetalleproducto;
   	 public $nombre_subproducto;
	 public $caracteristicas;
	 public $precio;
	 public $foto;
	  public $foto2;
	 public $idproducto;
	 public $idusuario;

	public function __CONSTRUCT()	{
		try{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Listar(){
		try{
			$result = array();

			$stm = $this->pdo->prepare(
"SELECT  p.idproducto as idproducto,p.nombre,nombre_subproducto,dp.iddetalleproducto,caracteristicas,precio,dp.foto as foto,usu.idusuario,usu.nomusu,usu.apeusu
from tbl_detalleProducto dp
left outer join tbl_productos p
on p.idproducto=dp.idproducto
left outer join tbl_usuario usu
on dp.idusuario=usu.idusuario");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	
	public function ListarComboPortafolio(){
		try{
			$result = array();

			$stm = $this->pdo->prepare(
			"SELECT * from tbl_portafolio");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function ListarCombousuario(){
		try{
			$result = array();

			$stm = $this->pdo->prepare(
			"SELECT * from tbl_usuario");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Obtener($id){
		try {
			$stm = $this->pdo->prepare(
				"SELECT * from tbl_productos where idproducto = ?");
			          

			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Obtener2($id){
		try {
			$stm = $this->pdo->prepare(
				"SELECT  p.idproducto as idproducto,p.nombre,nombre_subproducto,dp.iddetalleproducto,caracteristicas,precio,dp.foto as foto,dp.foto3,usu.idusuario,usu.nomusu,usu.apeusu
from tbl_detalleProducto dp
left outer join tbl_productos p
on p.idproducto=dp.idproducto
left outer join tbl_usuario usu
on dp.idusuario=usu.idusuario
where iddetalleproducto = ?");
			          

			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Eliminar($id){

		// seleccionamos la foto de la bd para elimnar
        $stmt_select = $this->pdo->prepare('SELECT foto,foto2 FROM tbl_detalleProducto WHERE iddetalleproducto =:prod_id');
        $stmt_select->execute(array(':prod_id'=>$id));
        $imgRow=$stmt_select->fetch(PDO::FETCH_ASSOC);
        unlink("detalle-producto-images/".$imgRow['foto']);
        unlink("detalle-producto-images/".$imgRow['foto2']);


		try{
			$stm = $this->pdo->prepare("DELETE FROM tbl_detalleProducto WHERE iddetalleproducto = ?");			          

			$stm->execute(array($id));

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Actualizar($data){
		try {
			$sql = "UPDATE tbl_detalleProducto SET 
						nombre_subproducto =?
						caracteristicas           = ?,						
						precio             = ?,
						foto    = ?,
						foto2    = ?
						idusuario    = ?
						
						WHERE iddetalleproducto = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                        $data->nombre_subproducto,
                        $data->caracteristicas,
                        $data->precio,
                        $data->foto,
                        $data->foto2,
                        $data->idusuario, 

                        $data->iddetalleproducto,
					)
				);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Registrar(detalleProducto $data)	{
		try {
		$sql = "INSERT into tbl_detalleProducto(nombre_subproducto,caracteristicas,precio,foto,foto2,idproducto,idusuario) values(?,?,?,?,?,?,?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
					$data->nombre_subproducto,
                    $data->caracteristicas,
                    $data->precio,
                    $data->foto,
                    $data->foto2,
                    $data->idproducto,
                    $data->idusuario
                    // $data->idpago                 
                    // date('Y-m-d')
                )
			);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}


}