<?php
class portafolio{
	private $pdo;

    public $idportafolio;
	public $foto;
	public $foto2;
	public $foto3;
	public $descripcion;
	public $idusuario;

	public function __CONSTRUCT()	{
		try{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Listar(){
		try{
			$result = array();

			$stm = $this->pdo->prepare(
			"SELECT * from tbl_portafolio po
inner join tbl_usuario u
on u.idusuario=po.idusuario");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function ListarCombouUsuarios(){
		try{
			$result = array();

			$stm = $this->pdo->prepare(
			"SELECT * from tbl_usuario");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function ListarRegistros(){
		try{
			$result = array();

			$stm = $this->pdo->prepare(
			"SELECT count(*) as totalportafolio from tbl_portafolio");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	} 
	public function Obtener($id){
		try {
			$stm = $this->pdo->prepare(
				"SELECT * from tbl_portafolio po
inner join tbl_usuario u
on u.idusuario=po.idusuario where idportafolio = ?");
			          

			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Eliminar($id){


		// seleccionamos la foto de la bd para elimnar
        $stmt_select = $this->pdo->prepare('SELECT foto,foto2,foto3 FROM tbl_portafolio WHERE idportafolio =:slid_id');
        $stmt_select->execute(array(':slid_id'=>$id));
        $imgRow=$stmt_select->fetch(PDO::FETCH_ASSOC);
        unlink("portafolio-images/".$imgRow['foto']);
        unlink("portafolio-images/".$imgRow['foto2']);
        unlink("portafolio-images/".$imgRow['foto3']);
		try{
			$stm = $this->pdo->prepare("DELETE FROM tbl_portafolio WHERE idportafolio = ?");			          

			$stm->execute(array($id));

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Actualizar($data){
		try {
			$sql = "UPDATE tbl_portafolio SET						
                        foto         = ?,
                        foto2        = ?,
                        foto3        = ?,
                        descripcion  = ?,
                        idusuario    = ?

				    WHERE idportafolio   = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                        $data->foto,
                        $data->foto2,
                        $data->foto3,
						$data->descripcion,
						$data->idusuario,

                        $data->idportafolio,
					)
				);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Registrar(portafolio $data)	{
		try {
		$sql = "INSERT into tbl_portafolio(foto,foto2,foto3,descripcion,idusuario) values(?,?,?,?,?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
                    $data->foto,
                    $data->foto2,
                    $data->foto3,
                    $data->descripcion,
                    $data->idusuario
                    // $data->idpago                 
                    // date('Y-m-d')
                )
			);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}


}
