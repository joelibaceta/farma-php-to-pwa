<?php
class video{
	private $pdo;

  public $idvideo;
  public $titulo; 
	public $url; 
	public $portafolio_idportafolio; 

	public function __CONSTRUCT()	{
		try{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	 public function Listar(){
		try{
			$result = array();

			$stm = $this->pdo->prepare(
			"SELECT po.idportafolio, descripcion,idvideo,url,titulo
from tbl_portafolio po
inner join tbl_video u
on u.portafolio_idportafolio=po.idportafolio");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	} 
	public function ListarPortafolios(){
		try{
			$result = array();

			$stm = $this->pdo->prepare(
			"SELECT * from tbl_portafolio");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	} 
	
	public function Obtener($id){
		try {
			$stm = $this->pdo->prepare(
				"SELECT po.idportafolio, descripcion,idvideo,url,titulo
from tbl_portafolio po
inner join tbl_video u
on u.portafolio_idportafolio=po.idportafolio

				where idvideo = ?");
			          

			$stm->execute(array($id)); 
			return $stm->fetch(PDO::FETCH_OBJ);

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Eliminar($id){
 
		try{
			$stm = $this->pdo->prepare("DELETE FROM tbl_video WHERE idvideo = ?");			          

			$stm->execute(array($id));

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Actualizar($data){
		try {
			$sql = "UPDATE tbl_video SET						
                        titulo         = ?,
                        url         = ?,
                        portafolio_idportafolio  = ?

				    WHERE idvideo   = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                        $data->titulo,
                        $data->url,
						$data->portafolio_idportafolio,

                        $data->idvideo,
					)
				);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Registrar(video $data)	{
		try {
		$sql = "INSERT into tbl_video(titulo,url,portafolio_idportafolio) values(?,?,?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
                    $data->titulo,
                    $data->url,
                    $data->portafolio_idportafolio
                    // $data->idpago                 
                    // date('Y-m-d')
                )
			);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}


}