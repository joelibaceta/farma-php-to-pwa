<?php
class productos{
	private $pdo;
    
    public $idproducto;
	public $nombre;
	public $color_nombre;
	public $subtitulo;
	public $descripcion;
	public $precio;
	public $foto;
	public $foto2;
	public $fecharegistro;
	public $idusuario; 
	public $idportafolio; 
	
	public $iddetalleproducto;
   	 public $nombre_subproducto;
	 public $caracteristicas;

	public function __CONSTRUCT()	{
		try{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Listar(){
		try{
			$result = array();

			$stm = $this->pdo->prepare(
			"SELECT idproducto,nombre,color_nombre,subtitulo,p.foto,p.foto2,fecharegistro,pf.idportafolio,pf.descripcion as marca
from tbl_productos p
inner join tbl_portafolio pf
on pf.idportafolio=p.idportafolio");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	
	public function ListarComboPortafolio(){
		try{
			$result = array();

			$stm = $this->pdo->prepare(
			"SELECT * from tbl_portafolio");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function ListarCombousuario(){
		try{
			$result = array();

			$stm = $this->pdo->prepare(
			"SELECT * from tbl_usuario");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	// funcion para el editar
	public function Obtener3($id){
		try {
			$stm = $this->pdo->prepare(
				"SELECT  p.idproducto as idproducto,p.nombre,nombre_subproducto,dp.iddetalleproducto,caracteristicas,precio,dp.foto as foto,dp.foto2,usu.idusuario,usu.nomusu,usu.apeusu
from tbl_detalleProducto dp
left outer join tbl_productos p
on p.idproducto=dp.idproducto
left outer join tbl_usuario usu
on dp.idusuario=usu.idusuario
where iddetalleproducto = ?");
			          

			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}
	
	// funcion para el agregar
public function Obtener2($id){
		try {
			$stm = $this->pdo->prepare(
				"SELECT * from tbl_productos where idproducto = ?");
			          

			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}
	public function Obtener($id){
		try {
			$stm = $this->pdo->prepare(
				"SELECT idproducto,nombre,color_nombre,subtitulo,p.descripcion as descripcion ,p.foto as fotopro,p.foto2 as fotopro2,fecharegistro,pf.idportafolio,pf.descripcion as marca
from tbl_productos p
inner join tbl_portafolio pf
on pf.idportafolio=p.idportafolio
				where idproducto = ?");
			          

			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Eliminar($id){

		// seleccionamos la foto de la bd para elimnar
        $stmt_select = $this->pdo->prepare('SELECT foto,foto2 FROM tbl_productos WHERE idproducto =:prod_id');
        $stmt_select->execute(array(':prod_id'=>$id));
        $imgRow=$stmt_select->fetch(PDO::FETCH_ASSOC);
        unlink("producto-images/".$imgRow['foto']);
        unlink("producto-images/".$imgRow['foto2']);


		try{
			$stm = $this->pdo->prepare("DELETE FROM tbl_productos WHERE idproducto = ?");			          

			$stm->execute(array($id));

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}
	
	public function Eliminar2($id){

		// seleccionamos la foto de la bd para elimnar
        $stmt_select = $this->pdo->prepare('SELECT foto,foto2 FROM tbl_detalleProducto WHERE iddetalleproducto =:prod_id');
        $stmt_select->execute(array(':prod_id'=>$id));
        $imgRow=$stmt_select->fetch(PDO::FETCH_ASSOC);
        unlink("detalle-producto-images/".$imgRow['foto']);
        unlink("detalle-producto-images/".$imgRow['foto2']);


		try{
			$stm = $this->pdo->prepare("DELETE FROM tbl_detalleProducto WHERE iddetalleproducto = ?");			          

			$stm->execute(array($id));

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Actualizar($data){
		try {
			$sql = "UPDATE tbl_productos SET 
						nombre        = ?,
						color_nombre  = ?,
						subtitulo     = ?,
						descripcion   = ?,
						foto          = ?,
						foto2         = ?,
						fecharegistro = ?,
						idportafolio  = ?
						
						WHERE idproducto = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                        
                        $data->nombre,
                        $data->color_nombre,
                        $data->subtitulo,
                        $data->descripcion,
                        $data->foto,
                        $data->foto2,
                        $data->fecharegistro,
                        $data->idportafolio,

                        $data->idproducto,
					)
				);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}
	
	// editar el detalle de producto
		public function Actualizarsub($data){
		try {
			$sql = "UPDATE tbl_detalleProducto SET 
						nombre_subproducto =?,
						caracteristicas    = ?,
						precio             = ?,
						foto               = ?,
						foto2              = ?,
						idusuario          = ?
						
						WHERE iddetalleproducto = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                        $data->nombre_subproducto,
                        $data->caracteristicas,
                        $data->precio,
                        $data->foto,
                        $data->foto2,
                        $data->idusuario, 

                        $data->iddetalleproducto,
					)
				);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}
	
	public function Registrarsub(productos $data)	{
		try {
		$sql = "INSERT into tbl_detalleProducto(nombre_subproducto,caracteristicas,precio,foto,foto2,idproducto,idusuario) values(?,?,?,?,?,?,?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
					$data->nombre_subproducto,
                    $data->caracteristicas,
                    $data->precio,
                    $data->foto,
                    $data->foto2,
                    $data->idproducto,
                    $data->idusuario
                    // $data->idpago                 
                    // date('Y-m-d')
                )
			);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Registrar(productos $data)	{
		try {
		$sql = "INSERT into tbl_productos(nombre,color_nombre,subtitulo,descripcion,foto,foto2,fecharegistro,idportafolio) values(?,?,?,?,?,?,?,?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
                    $data->nombre,
                    $data->color_nombre,
                    $data->subtitulo,
                    $data->descripcion,
                    $data->foto,
                    $data->foto2,
                    $data->fecharegistro,
                    $data->idportafolio
                    // $data->idpago                 
                    // date('Y-m-d')
                )
			);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}


}