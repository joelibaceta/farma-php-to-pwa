<?php
class usuario{
	private $pdo;
    
    public $idusuario;
    public $dni;
    public $nomusu;
    public $apeusu;
    public $corrusu;
    public $clausu;
    public $perfil_idperfil;

	public function __CONSTRUCT()	{
		try{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Listar(){
		try{
			$result = array();

			$stm = $this->pdo->prepare(
			"SELECT idusuario,dni,nomusu,apeusu,corrusu,clausu,p.idperfil,p.nomperfil
			from tbl_usuario u 
			inner join tbl_perfiles p
			on u.perfil_idperfil=p.idperfil");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function ListarComboPerfil(){
		try{
			$result = array();

			$stm = $this->pdo->prepare(
			"SELECT * from tbl_perfiles ");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Obtener($id){
		try {
			$stm = $this->pdo->prepare(
				"SELECT idusuario,dni,nomusu,apeusu,corrusu,clausu,p.idperfil,p.nomperfil
				from tbl_usuario u 
				inner join tbl_perfiles p
				on u.perfil_idperfil=p.idperfil
				where idusuario= ?");
			          

			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Eliminar($id){
		try{
			$stm = $this->pdo->prepare("DELETE FROM tbl_usuario WHERE idusuario = ?");			          

			$stm->execute(array($id));

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Actualizar($data){
		try {
			$sql = "UPDATE tbl_usuario SET 
						dni              = ?,
						nomusu           = ?, 
						apeusu         = ?,
                        corrusu        = ?,
                        clausu           = ?,
                        perfil_idperfil         = ?

				    WHERE idusuario             = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                        
                        $data->dni,
						$data->nomusu,
						$data->apeusu,
                        $data->corrusu,
                        $data->clausu,
                        $data->perfil_idperfil,

                        $data->idusuario,
					)
				);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Registrar(usuario $data)	{
		try {
		$sql = "INSERT into tbl_usuario(dni,nomusu,apeusu,corrusu,clausu,perfil_idperfil) values(?,?,?,?,?,?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
                    $data->dni,
					$data->nomusu,
					$data->apeusu,
                    $data->corrusu,
                    $data->clausu,
                    $data->perfil_idperfil
                    // $data->idpago                 
                    // date('Y-m-d')
                )
			);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}


}