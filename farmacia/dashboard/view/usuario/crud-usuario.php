<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li>usuarios</li>
            <li class="active">Agregar Nuevo Usuario</li>
        </ul>
        <!--breadcrumbs end -->
	    <h1 class="h1"></h1>
        <h1 > <strong><?php echo $usu->idusuario != null ? "Actualizar Usuario" : 'Nuevo Usuario'; ?></strong> </h1>
	</div>
</div>

<div class="container">
<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Matenimiento de los Usuarios</h3>
            <div class="actions pull-right">
                <i class="fa fa-chevron-down"></i>
                <i class="fa fa-times"></i>
            </div>
        </div>
        <div class="panel-body">          
            <div class="col-md-6 col-md-offset-3">
            <form action="?c=usuario&a=Guardar" method="post" class="form-vertical">
            <!-- id Usuario --> <input type="hidden" name="txtCodUsuario" id="txtCodUsuario" value="<?php echo $usu->idusuario; ?>">
                <div class="">
                    <div class="form-group">
                        <label for="txtDni">DNI</label>
                        <input type="text" name="txtDni" id="txtDni" maxlength="8" class="form-control inputs" value="<?php echo $usu->dni; ?>" placeholder="ejem: 87654321">
                    </div>
                </div>
                <div class="">
                    <div class="form-group">
                        <label for="txtNombre">NOMBRES</label>
                        <input type="text" name="txtNombre" id="txtNombre" class="form-control inputs" value="<?php echo $usu->nomusu; ?>" placeholder="ejem: Juan Gabriel">
                    </div>
                </div>
                <div class="">
                    <div class="form-group">
                        <label for="txtApellido">APELLIDOS</label>
                        <input type="text" name="txtApellido" id="txtApellido" class="form-control inputs" value="<?php echo $usu->apeusu; ?>" placeholder="ejem: Perez Gutierrez">
                    </div>
                </div>
                <div class="">
                    <div class="form-group">
                        <label for="txtCorreo">CORREO</label>
                        <input type="text" name="txtCorreo" id="txtCorreo" class="form-control inputs" value="<?php echo $usu->corrusu; ?>" placeholder="ejem: juanperez@hotmail.com">
                    </div>
                </div>
                <div class="">
                    <div class="form-group">
                        <label for="txtClave">CONTRASEÑA</label>
                        <input type="text" name="txtClave" id="txtClave" class="form-control inputs" value="<?php echo $usu->clausu; ?>" placeholder="ejem: juan1997">
                    </div>
                </div>
                <div class="">
                    <div class="form-group">
                        <label for="cboPerfil">PERFIL</label> 
                        <select name="cboPerfil" id="cboPerfil" class="form-control inputs">
                            <option value="">Seleciona un Perfíl</option>
                            <?php foreach($this->model->ListarComboPerfil() as $r): ?>                       
                                <option value="<?php echo $r->idperfil; ?>"><?php echo $r->nomperfil; ?></option>
                            <?php endforeach; ?>

                            <?php echo $usu->idusuario != null ? '  
                            <option value="'. $usu->idperfil . '" selected="" >'. $usu->nomperfil . '</option>
                           ' : '
                       
                            '; ?>
                            
                        </select>
                    </div>
                </div>
                <div class="text-center">
                    <div class="form-group">
                    <br><br>
                        <a href="usuarioindex.php" class="btn btn-default">Cancelar</a>
                        <input type="submit" class="btn btn-success" id="GuardarUsuario" value="Guardar">
                    </div>
                </div>
            </form>    
            </div>
       </div>
    </div>
</div> 

</div>

<script src="js/validar-usuario.js"></script>