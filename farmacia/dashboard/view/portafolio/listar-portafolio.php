 <div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li>portafolio</li>
            <li class="active">Todos los portafolio</li>
        </ul>
        <!--breadcrumbs end -->
	    <h1 class="h1">portafolio</h1>
	</div>
</div>
<style>
	.imagen-portafolio{
		max-width: 100%;
		height: 150px;

	}
</style>
<div class="">
	<a class="btn btn-primary pull-right" href="?c=portafolio&a=Crud">Registrar Nuevo portafolio</a>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
          	<div class="panel-heading">
	            <h3 class="panel-title">Listado General de todos los portafolio</h3>
	            <div class="actions pull-right">
	                <i class="fa fa-chevron-down"></i>
	                <i class="fa fa-times"></i>
	            </div>
          	</div>
          	<div class="panel-body">          
	            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
	            <thead>
	                <tr>
	                    <th>#</th>
	                    <th >Logo de la Marca</th>
	                    <th >Banner de la marca</th>
	                    <th>Razón social</th>
	                    <th>Empleado</th>
	                    <th class="text-center">Acciones</th>
	                </tr>
	            </thead>
	     
	            <tbody>
	            <?php $cont=0; ?>
	            <?php foreach($this->model->Listar() as $r): ?>
	                <tr>
	                    <td><?php echo $cont=$cont +1; ?></td>
	                    <td ><img src="portafolio-images/<?php echo $r->foto; ?>" alt="" class="img-responsive imagen-portafolio"></td>
	                    <td ><img src="portafolio-images/<?php echo $r->foto2; ?>" alt="" class="img-responsive imagen-portafolio"></td>
	                    <td><?php echo $r->descripcion; ?></td>
	                    <td><?php echo $r->nomusu.' '. $r->apeusu; ?></td>
	                    <td class="text-center">
			                <a  class="btn btn-xs btn-info" href="?c=portafolio&a=Crud&id=<?php echo $r->idportafolio; ?>">Editar</a>

			                <a  class="btn btn-xs btn-danger" style="color: #fff;" onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" href="?c=portafolio&a=Eliminar&id=<?php echo $r->idportafolio; ?>">Eliminar</a>
			            </td>	
	                </tr>
	            <?php endforeach; ?>   
	            </tbody>
	            </table>

           </div>
        </div>
    </div>
</div>