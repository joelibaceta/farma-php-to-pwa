<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li>portafolio</li>
            <li class="active">Agregar Nuevo portafolio</li>
        </ul>
        <!--breadcrumbs end -->
	    <h1 class="h1"></h1>
        <h1 > <strong><?php echo $porta->idportafolio != null ? "Actualizar portafolio" : 'Nuevo portafolio'; ?></strong> </h1>
	</div>
</div>
<style>
	.imagen-portafolio{
		max-width: 100%;
		height: 100%x;
		width: 100%;

	}
</style>

<div class="container">
<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Matenimiento de los portafolio</h3>
            <div class="actions pull-right">
                <i class="fa fa-chevron-down"></i>
                <i class="fa fa-times"></i>
            </div>
        </div>
        <div class="panel-body">          
            <div class="col-md-10 col-md-offset-1">
            <form action="?c=portafolio&a=Guardar" method="post" class="form-vertical" enctype="multipart/form-data">
            <!-- id portafolio --> <input type="hidden" name="txtCodportafolio" id="txtCodportafolio" class="form-control" value="<?php echo $porta->idportafolio; ?>">
            <?php $cont=0; ?>
            <?php foreach($this->model->ListarRegistros() as $r): ?>
                <?php $cont=$cont+1; 
                $tot = $r->totalportafolio; ?>
             <?php endforeach; ?> 
                <input type="hidden" name="txtIdSlid" id="txtIdSlid" class="form-control" value="<?php echo $tot+1; ?>" >
                <div class="">
                    <div class="form-group">
                        <label for="txtDescripcion">RAZON SOCIAL DE LA MARCA</label>
                        <input name="txtDescripcion" id="txtDescripcion" class="form-control inputs" placeholder="ejem: enetperu SAC" value="<?php echo $porta->descripcion ; ?>">
                        
                    </div>
                </div>
                <div class="row form-group">
                    <div class="form-group">                       
                        <div class="col-md-8">
                           <?php if ($porta->idportafolio != null){ ?>
                                <label for="txtFotos">LOGO DE LA MARCA</label>
                                <img src="portafolio-images/<?php echo $porta->foto; ?>" alt="" class="img-responsive  imagen-portafolio">
                                <input type="file"  name="txtFoto" id="txtFoto" accept="image/*" class="inputs">
                                <small>(maximo 1MB)</small>
                            <?php } else{ ?>
                                <label for="txtFotos">LOGO DE LA MARCA</label>
                                <div id="imgPreview"></div>
                                <input type="file"  name="txtFoto" id="txtFoto" accept="image/*" class="inputs">
                                <small>(maximo 1MB)</small>
                            <?php }?> 
                        </div>                        
                    </div>
                </div>
                <div class="row form-group">
                    <div class="form-group">                       
                        <div class="col-md-8">
                           <?php if ($porta->idportafolio != null){ ?>
                                <label for="txtFotos">BANNER DEL PORTAFOLIO</label>
                                <img src="portafolio-images/<?php echo $porta->foto2; ?>" alt="" class="img-responsive  imagen-portafolio">
                                <input type="file"  name="txtFoto2" id="txtFoto2" accept="image/*" class="inputs">
                                <small>(maximo 1MB)</small>
                            <?php } else{ ?>
                                <label for="txtFoto2s">BANNER DEL PORTAFOLIO</label>
                                <div id="imgPreview2"></div>
                                <input type="file"  name="txtFoto2" id="txtFoto2" accept="image/*" class="inputs">
                                <small>(maximo 1MB)</small>
                            <?php }?> 
                        </div>                        
                    </div>
                </div>
                <div class="row form-group">
                    <div class="form-group">                       
                        <div class="col-md-8">
                           <?php if ($porta->idportafolio != null){ ?>
                                <label for="txtFotos">TEXTURA DEL PORTAFOLIO</label>
                                <img src="portafolio-images/<?php echo $porta->foto3; ?>" alt="" class="img-responsive  imagen-portafolio">
                                <input type="file"  name="txtFoto3" id="txtFoto3" accept="image/*" class="inputs">
                                <small>(maximo 1MB)</small>
                            <?php } else{ ?>
                                <label for="txtFoto3s">TEXTURA DEL PORTAFOLIO</label>
                                <div id="imgPreview3"></div>
                                <input type="file"  name="txtFoto3" id="txtFoto3" accept="image/*" class="inputs">
                                <small>(maximo 1MB)</small>
                            <?php }?> 
                        </div>                        
                    </div>
                </div>
                <div class="">
                    <div class="form-group">
                        <label for="cboUsuario">EMPLEADO / VENDEDOR</label> 
                        <select name="cboUsuario" id="cboUsuario" class="form-control inputs">
                            <option value="">Seleciona un Usuario</option>
                            <?php foreach($this->model->ListarCombouUsuarios() as $r): ?>                       
                                <option value="<?php echo $r->idusuario; ?>"><?php echo $r->nomusu .' '. $r->apeusu; ?></option>
                            <?php endforeach; ?>

                            <?php echo $porta->idusuario != null ? '  
                            <option value="'. $porta->idusuario . '" selected="" >'. $porta->nomusu .' '. $porta->apeusu . '</option>
                           ' : '
                       
                            '; ?>
                            
                        </select>
                    </div>
                </div>
                <div class="text-center">
                    <div class="form-group">
                    <br><br>
                        <a href="portafolioindex.php" class="btn btn-default">Cancelar</a>
                        <input type="submit" class="btn btn-success" id="GuardarPortafolio" value="Guardar">
                    </div>
                </div>
            </form>    
            </div>
       </div>
    </div>
</div> 

</div>
<script src="js/validar-portafolio.js"></script>
<script>
	(function(){
		function filepreview(input){
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function(e){
					$('#imgPreview').html('<img src="'+e.target.result+'" class="img-responsive img-rounded" />')
				}
				reader.readAsDataURL(input.files[0]);
			}
		}

		$('#txtFoto').change(function(){
			filepreview(this);
		});
        // para el banner de la marca
        function filepreview2(input){
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e){
                    $('#imgPreview2').html('<img src="'+e.target.result+'" class="img-responsive img-rounded" />')
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#txtFoto2').change(function(){
            filepreview2(this);
        });
        // para el TEXTURA de la marca
        function filepreview3(input){
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e){
                    $('#imgPreview3').html('<img src="'+e.target.result+'" class="img-responsive img-rounded" />')
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#txtFoto3').change(function(){
            filepreview3(this);
        });
	})();
</script>