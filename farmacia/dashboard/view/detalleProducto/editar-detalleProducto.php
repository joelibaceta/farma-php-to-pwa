<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li>Productos</li>
            <li class="active">Editar Detalle de Productos</li>
        </ul>
        <!--breadcrumbs end -->
        <!-- <h1 class="h1"> <strong>Producto :</strong> <?php echo $pro->nombre; ?></h1> -->
         <h1 > <strong><?php echo $pro->iddetalleproducto != null ? "Actualizar detalle" : "Producto :". $pro->nombre; ?></strong> </h1>  
    </div>
</div>
<style>
    .imagen-slider, #imgPreview{
        max-width: 100%;
        height: 100%;
        width: 100%;
        margin-bottom: 5px;

    }
</style>

<div class="container">
<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Detalle del Producto</h3>
            <div class="actions pull-right">
                <i class="fa fa-chevron-down"></i>
                <i class="fa fa-times"></i>
            </div>
        </div>
        <div class="panel-body">          
            <div class="col-md-10 col-md-offset-1">
            <form action="?c=productos&a=Guardar3" method="post" class="form-vertical" enctype="multipart/form-data">
            <!-- id  Producto --> <input type="hidden" name="txtCodProducto" id="txtCodProducto"  value="<?php echo $pro->iddetalleproducto; ?>">
            <!-- id detalle Producto --> <input type="hidden" name="txtCoddetProducto" id="txtCoddetProducto"  value="<?php echo $pro->idproducto; ?>">
                <div class="">
                    <div class="form-group">
                        <label for="txtNomProducto">NOMBRE DEL PRODUCTO</label>
                        <input type="text" name="txtNomProducto" id="txtNomProducto" class="form-control inputs" value="<?php echo $pro->nombre_subproducto; ?>">
                    </div>
                </div>
                <div class="">
                    <div class="form-group">
                        <label for="txtDescripcion">CARACTERISTICAS DEL PRODUCTO</label>
                        <textarea name="txtDescripcion" id="txtDescripcion" class="form-control inputs"  cols="30" rows="10" placeholder="ejem: shampoo acondicionador de 1L"><?php echo $pro->caracteristicas; ?></textarea>
                        
                    </div>
                </div>
                <div class="">
                    <div class="form-group">
                        <label for="txtPrecio">PRECIO DEL PRODUCTO</label>
                        <input type="text" name="txtPrecio" id="txtPrecio" class="form-control inputs" value="<?php echo $pro->precio; ?>"  value="" placeholder="ejem: 28.50">
                    </div>
                </div>
                
                <div class="row form-group">
                    <div class="form-group">
                        <div class="col-md-8">
                            <label for="txtFotos">FOTO DEL PRODUCTO</label>
                            <img src="detalle-producto-images/<?php echo $pro->foto; ?> " alt="">
                            
                                <!-- <div id="imgPreview"></div> -->
                                <input type="file"  name="txtFoto" id="txtFoto" accept="image/*" class="inputs">
                                <small>(maximo 1MB)</small>
 
                        </div>                         
                    </div>
                </div>
                <div class="row form-group">
                    <div class="form-group">
                        <div class="col-md-8">
                            <label for="txtFotos">IMAGEN DE FONDO DEL PRECIO</label>
                            <img src="detalle-producto-images/<?php echo $pro->foto2; ?> " alt="">
                            <!-- <div id="imgPreview2"></div> -->
                            <input type="file"  name="txtFoto2" id="txtFoto2" accept="image/*" class="inputs">
                            <small>(maximo 1MB)</small>
                        </div>                         
                    </div>
                </div>
                <div class="">
                    <div class="form-group">
                        <label for="cboUsuario">EMPLEADO / VENDEDOR</label> 
                        <select name="cboUsuario" id="cboUsuario" class="form-control inputs">
                            <option value="">Seleciona un Usuario</option>
                            <?php foreach($this->model->ListarCombousuario() as $r): ?>                       
                                <option value="<?php echo $r->idusuario; ?>"><?php echo $r->nomusu .' '. $r->apeusu; ?></option>
                            <?php endforeach; ?>

                            <?php echo $pro->iddetalleproducto != null ? '  
                            <option value="'. $pro->idusuario . '" selected="" >'. $pro->nomusu .' '. $pro->apeusu . '</option>
                           ' : '
                       
                            '; ?>
                            
                        </select>
                    </div>
                </div>
               
                
                
                <div class="text-center">
                    <div class="form-group">
                    <br><br>
                        <a href="detalleproductoindex.php" class="btn btn-default">Cancelar</a>
                        <input type="submit" class="btn btn-success" id="GuardarDetallProducto" value="Guardar">
                    </div>
                </div>
            </form>    
            </div>
       </div>
    </div>
</div> 

</div>
<script src="js/validar-detalle-producto.js"></script>
<script>
    (function(){
        function filepreview(input){
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e){
                    $('#imgPreview').html('<img src="'+e.target.result+'" class="img-responsive img-rounded" />')
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#txtFoto').change(function(){
            filepreview(this);
        });
    })();
</script>