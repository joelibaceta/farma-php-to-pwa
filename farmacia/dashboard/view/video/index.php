 <div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li>video</li>
            <li class="active">Todos los video</li>
        </ul>
        <!--breadcrumbs end -->
	    <h1 class="h1">video</h1>
	</div>
</div>
<style>
	.imagen-portafolio{
		max-width: 100%;
		height: 150px;

	}
</style>
<div class="">
	<a class="btn btn-primary pull-right" href="?c=video&a=Crud">Registrar Nuevo video</a>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
          	<div class="panel-heading">
	            <h3 class="panel-title">Listado General de todos los videos</h3>
	            <div class="actions pull-right">
	                <i class="fa fa-chevron-down"></i>
	                <i class="fa fa-times"></i>
	            </div>
          	</div>
          	<div class="panel-body">          
	            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
	            <thead>
	                <tr>
	                    <th>#</th>	                    
	                    <th >titulo</th>
	                    <th >url</th>
	                    <th >Portafolio</th>
	                    <th class="text-center">Acciones</th>
	                </tr>
	            </thead>
	     
	            <tbody>
	            <?php $cont=0; ?>
	            <?php foreach($this->model->Listar() as $r): ?>
	                <tr>idvideo
 

	                    <td><?php echo $cont=$cont +1; ?></td>
	                    <td ><?php echo $r->titulo; ?></td>
	                    <td ><?php echo $r->url; ?></td>
	                    <td><?php echo $r->descripcion; ?></td>
	                    <td class="text-center">
			                <a  class="btn btn-xs btn-info" href="?c=video&a=Crud&id=<?php echo $r->idvideo; ?>">Editar</a>

			                <a  class="btn btn-xs btn-danger" style="color: #fff;" onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" href="?c=video&a=Eliminar&id=<?php echo $r->idvideo; ?>">Eliminar</a>
			            </td>	
	                </tr>
	            <?php endforeach; ?>   
	            </tbody>
	            </table>

           </div>
        </div>
    </div>
</div>