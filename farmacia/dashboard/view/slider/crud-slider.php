<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li>sliders</li>
            <li class="active">Agregar Nuevo slider</li>
        </ul>
        <!--breadcrumbs end -->
	    <h1 class="h1"></h1>
        <h1 > <strong><?php echo $slid->idslider != null ? "Actualizar slider" : 'Nuevo slider'; ?></strong> </h1>
	</div>
</div>
<style>
	.imagen-slider{
		max-width: 100%;
		height: 100%x;
		width: 100%;

	}
</style>

<div class="container">
<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Matenimiento de los sliders</h3>
            <div class="actions pull-right">
                <i class="fa fa-chevron-down"></i>
                <i class="fa fa-times"></i>
            </div>
        </div>
        <div class="panel-body">          
            <div class="col-md-10 col-md-offset-1">
            <form action="?c=slider&a=Guardar" method="post" class="form-vertical" enctype="multipart/form-data">
            <!-- id slider --> <input type="hidden" name="txtCodSlider" id="txtCodSlider" class="form-control" value="<?php echo $slid->idslider; ?>">
            <?php $cont=0; ?>
            <?php foreach($this->model->ListarRegistros() as $r): ?>
                <?php $cont=$cont+1; 
                $tot = $r->totalSlider; ?>
             <?php endforeach; ?> 
                <input type="hidden" name="txtIdSlid" id="txtIdSlid" class="form-control" value="<?php echo $tot+1; ?>" >
                <div class="">
                    <div class="form-group">
                        <label for="txtDescripcion">DESCRIPCIÓN DEL SLIDER</label>
                        <textarea name="txtDescripcion" id="txtDescripcion" class="form-control inputs" cols="30" rows="10" placeholder="ejem: shampoo acondicionador de 1L"><?php echo $slid->descripcion ; ?></textarea>
                        
                    </div>
                </div>
                <div class="row form-group">
                    <div class="form-group">                       
                        <div class="col-md-8">
                           <?php if ($slid->idslider != null){ ?>
                                <label for="txtFotos">FOTO DEL SLIDER</label>
                                <img src="slider-images/<?php echo $slid->foto; ?>" alt="" class="img-responsive  imagen-slider">
                                <input type="file"  name="txtFoto" id="txtFoto" accept="image/*" class="inputs" >
                                <small>(maximo 1MB)</small>
                            <?php } else{ ?>
                                <label for="txtFotos">FOTO DEL SLIDER</label>
                                <div id="imgPreview"></div>
                                <input type="file"  name="txtFoto" id="txtFoto" accept="image/*" >
                                <small>(maximo 1MB)</small>
                            <?php }?> 
                        </div>                        
                    </div>
                </div>
                <div class="text-center">
                    <div class="form-group">
                    <br><br>
                        <a href="sliderindex.php" class="btn btn-default">Cancelar</a>
                        <input type="submit" class="btn btn-success" id="GuardarSlider" value="Guardar">
                    </div>
                </div>
            </form>    
            </div>
       </div>
    </div>
</div> 

</div>
<script src="js/validar-slider.js"></script>
<script>
	(function(){
		function filepreview(input){
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function(e){
					$('#imgPreview').html('<img src="'+e.target.result+'" class="img-responsive img-rounded" />')
				}
				reader.readAsDataURL(input.files[0]);
			}
		}

		$('#txtFoto').change(function(){
			filepreview(this);
		});
	})();
</script>