 <div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li>sliders</li>
            <li class="active">Todos los sliders</li>
        </ul>
        <!--breadcrumbs end -->
	    <h1 class="h1">sliders</h1>
	</div>
</div>
<style>
	.imagen-slider{
		max-width: 100%;
		height: 150px;

	}
</style>
<div class="">
	<a class="btn btn-primary pull-right" href="?c=slider&a=Crud">Registrar Nuevo slider</a>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
          	<div class="panel-heading">
	            <h3 class="panel-title">Listado General de todos los sliders</h3>
	            <div class="actions pull-right">
	                <i class="fa fa-chevron-down"></i>
	                <i class="fa fa-times"></i>
	            </div>
          	</div>
          	<div class="panel-body">          
	            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
	            <thead>
	                <tr>
	                    <th>#</th>
	                    <th >Slider</th>
	                    <th>Descripción</th>
	                    <th class="text-center">Acciones</th>
	                </tr>
	            </thead>
	     
	            <tbody>
	            <?php $cont=0; ?>
	            <?php foreach($this->model->Listar() as $r): ?>
	                <tr>
	                    <td><?php echo $cont=$cont +1; ?></td>
	                    <td ><img src="slider-images/<?php echo $r->foto; ?>" alt="" class="img-responsive imagen-slider"></td>
	                    <td><?php echo $r->descripcion; ?></td>
	                    <td class="text-center">
			                <a  class="btn btn-xs btn-info" href="?c=slider&a=Crud&id=<?php echo $r->idslider; ?>">Editar</a>

			                <a  class="btn btn-xs btn-danger" style="color: #fff;" onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" href="?c=slider&a=Eliminar&id=<?php echo $r->idslider; ?>">Eliminar</a>
			            </td>	
	                </tr>
	            <?php endforeach; ?>   
	            </tbody>
	            </table>

           </div>
        </div>
    </div>
</div>