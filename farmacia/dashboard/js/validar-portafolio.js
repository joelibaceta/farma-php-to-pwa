var expr1 = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
var expr2 = /^[0-9]+$/;
$("#GuardarPortafolio").click(function() {
    var descripcion      = $("#txtDescripcion").val();
    var logo   = $("#txtFoto").val();
    var banner = $("#txtFoto2").val();
    var usuario   = $("#cboUsuario").val();
    // validamos
    if (descripcion == "" && logo == ""&& banner == "" && usuario == "" ) {
        $("input#txtDescripcion").focus();
        sweetAlert("Oops...", "Ingrese todos los Campos Requeridos", "warning");
        $(".inputs").css({
            "border": "solid 1px #a8272d",
        });
        $(".vehiculo, .viajar, .recidencia, .generooo ").css({
            "color": "#a8272d",
        });
        return false;
    } else if (descripcion == "") {
        $(".inputs").css({
            "border": "",
        });
        $("input#txtDescripcion").focus();
        sweetAlert("Oops...", "Digite la marca ", "warning");
        $("#txtDescripcion").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    } else if (logo == "") {
        $(".inputs").css({
            "border": "",
        });
        $("input#txtFoto").focus();
        sweetAlert("Oops...", "Seleccione el logo", "warning");
        $("#txtFoto").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    }else if (banner == "") {
        $(".inputs").css({
            "border": "",
        });
        $("input#txtFoto2").focus();
        sweetAlert("Oops...", "Seleccione el banner de la marca", "warning");
        $("#txtFoto2").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    }  else if (usuario == "") {
        $(".inputs").css({
            "border": "",
        });
        $("input#cboUsuario").focus();
        sweetAlert("Oops...", "Seleccione el vendedor", "warning");
        $("#cboUsuario").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    } else {
        form.submit();
    }
});