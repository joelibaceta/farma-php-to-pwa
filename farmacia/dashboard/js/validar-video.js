var expr1 = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
var expr2 = /^[0-9]+$/;
$("#GuardarVideo").click(function() {
    var nombre      = $("#txtTitulo").val();
    var url   = $("#txtUrl").val();
    var portafolio   = $("#cboPortafolio").val();
    // validamos
    if (nombre == ""  && url == "" &&  portafolio == "") {
        $("input#txtTitulo").focus();
        sweetAlert("Oops...", "Ingrese todos los Campos Requeridos", "warning");
        $(".inputs").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    } else if (nombre == "" ) {
        $(".inputs").css({
            "border": "",
        });
        $("input#txtTitulo").focus();
        sweetAlert("Oops...", "Digite el nombre del video", "warning");
        $("#txtTitulo").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    } else if (url == "") {
        $(".inputs").css({
            "border": "",
        });
        $("input#txtUrl").focus();
        sweetAlert("Oops...", "Digite la url del video", "warning");
        $("#txtUrl").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    } else if (portafolio == "") {
        $(".inputs").css({
            "border": "",
        });
        $("select#cboPortafolio").focus();
        sweetAlert("Oops...", "Seleccione el portafolio para el video", "warning");
        $("#cboPortafolio").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    }  else {
        form.submit();
    }
});