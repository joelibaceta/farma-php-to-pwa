var expr1 = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
var expr2 = /^[0-9]+$/;
$("#GuardarDetallProducto").click(function() {
    var nombre      = $("#txtNomProducto").val();
    var Descripcion      = $("#txtDescripcion").val();
    var Precio   = $("#txtPrecio").val();
    var Foto = $("#txtFoto").val();
    var Usuario   = $("#cboUsuario").val(); 
    // validamos
    if (nombre == "" &&  Precio == ""&& !expr2.test(Precio) && Foto == "" && Usuario == "" ) {
        $("input#txtNomProducto").focus();
        sweetAlert("Oops...", "Ingrese todos los Campos Requeridos", "warning");
        $(".inputs").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    } else if (nombre == "" ) {
        $(".inputs").css({
            "border": "",
        });
        $("input#txtNomProducto").focus();
        sweetAlert("Oops...", "Digite nombre del producto", "warning");
        $("#txtNomProducto").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    } else if (Precio == "" ) {
        $(".inputs").css({
            "border": "",
        });
        $("input#txtPrecio").focus();
        sweetAlert("Oops...", "Digite el precio del producto \n solo numeros", "warning");
        $("#txtPrecio").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    }else if (Foto == "") {
        $(".inputs").css({
            "border": "",
        });
        $("input#txtFoto").focus();
        sweetAlert("Oops...", "selecciona una imagen del producto", "warning");
        $("#txtFoto").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    }  else if (Usuario == "" ) {
        $(".inputs").css({
            "border": "",
        });
        $("select#cboUsuario").focus();
        sweetAlert("Oops...", "selecciona a un vendedor", "warning");
        $("#cboUsuario").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    }  else {
        form.submit();
    }
});