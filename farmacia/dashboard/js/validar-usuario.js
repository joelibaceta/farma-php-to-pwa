var expr1 = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
var expr2 = /^[0-9]+$/;
$("#GuardarUsuario").click(function() {
    var dni      = $("#txtDni").val();
    var nombre   = $("#txtNombre").val();
    var apellido = $("#txtApellido").val();
    var correo   = $("#txtCorreo").val();
    var clave    = $("#txtClave").val();
    var perfil   = $("#cboPerfil").val();
    // validamos
    if (nombre == "" && clave == "" && perfil == "" ) {
        $("input#txtDni").focus();
        sweetAlert("Oops...", "Ingrese todos los Campos Requeridos", "warning");
        $(".inputs").css({
            "border": "solid 1px #a8272d",
        });
        $(".vehiculo, .viajar, .recidencia, .generooo ").css({
            "color": "#a8272d",
        });
        return false;
    } else if (nombre == "") {
        $(".inputs").css({
            "border": "",
        });
        $("input#txtNombre").focus();
        sweetAlert("Oops...", "Digite los  nombres completos", "warning");
        $("#txtNombre").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    } else if (clave == ""  ) {
        $(".inputs").css({
            "border": "",
        });
        $("input#txtClave").focus();
        sweetAlert("Oops...", "Digite una contraseña para el usuario", "warning");
        $("#txtClave").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    } else if (perfil == "") {
        $(".inputs").css({
            "border": "",
        });
        $("select#cboPerfil").focus();
        sweetAlert("Oops...", "Seleccione el perfil para el usuario", "warning");
        $("#cboPerfil").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    }  else {
        form.submit();
    }
});