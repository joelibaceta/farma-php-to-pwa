            </section>
        </section>
        <!--main content end-->

        <!--sidebar right start-->
        <aside class="sidebarRight">
            <div id="rightside-navigation ">
                <div class="sidebar-heading"><i class="fa fa-user"></i> Contacts</div>
                <div class="sidebar-title">online</div>
                <div class="list-contacts">
                    <a href="javascript:void(0)" class="list-item">
                        <div class="list-item-image">
                            <img src="../assets/img/avatar.gif" class="img-circle">
                        </div>
                        <div class="list-item-content">
                            <h4>James Bagian</h4>
                            <p>Los Angeles, CA</p>
                        </div>
                        <div class="item-status item-status-online"></div>
                    </a>
                    <a href="javascript:void(0)" class="list-item">
                        <div class="list-item-image">
                            <img src="../assets/img/avatar1.gif" class="img-circle">
                        </div>
                        <div class="list-item-content">
                            <h4>Jeffrey Ashby</h4>
                            <p>New York, NY</p>
                        </div>
                        <div class="item-status item-status-online"></div>
                    </a>
                    <a href="javascript:void(0)" class="list-item">
                        <div class="list-item-image">
                            <img src="../assets/img/avatar2.gif" class="img-circle">
                        </div>
                        <div class="list-item-content">
                            <h4>John Douey</h4>
                            <p>Dallas, TX</p>
                        </div>
                        <div class="item-status item-status-online"></div>
                    </a>
                    <a href="javascript:void(0)" class="list-item">
                        <div class="list-item-image">
                            <img src="../assets/img/avatar3.gif" class="img-circle">
                        </div>
                        <div class="list-item-content">
                            <h4>Ellen Baker</h4>
                            <p>London</p>
                        </div>
                        <div class="item-status item-status-away"></div>
                    </a>
                </div>

                <div class="sidebar-title">offline</div>
                <div class="list-contacts">
                    <a href="javascript:void(0)" class="list-item">
                        <div class="list-item-image">
                            <img src="../assets/img/avatar4.gif" class="img-circle">
                        </div>
                        <div class="list-item-content">
                            <h4>Ivan Bella</h4>
                            <p>Tokyo, Japan</p>
                        </div>
                        <div class="item-status"></div>
                    </a>
                    <a href="javascript:void(0)" class="list-item">
                        <div class="list-item-image">
                            <img src="../assets/img/avatar5.gif" class="img-circle">
                        </div>
                        <div class="list-item-content">
                            <h4>Gerald Carr</h4>
                            <p>Seattle, WA</p>
                        </div>
                        <div class="item-status"></div>
                    </a>
                    <a href="javascript:void(0)" class="list-item">
                        <div class="list-item-image">
                            <img src="../assets/img/avatar6.gif" class="img-circle">
                        </div>
                        <div class="list-item-content">
                            <h4>Viktor Gorbatko</h4>
                            <p>Palo Alto, CA</p>
                        </div>
                        <div class="item-status"></div>
                    </a>
                </div>
            </div>
        </aside>
        <!--sidebar right end-->
    </section>
    <!--Global JS-->
     <!-- <script src="../assets/js/jquery-1.10.2.min.js"></script> -->
        <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/plugins/waypoints/waypoints.min.js"></script>
    <script src="../assets/plugins/nanoScroller/jquery.nanoscroller.min.js"></script>
    <script src="../assets/js/application.js"></script>
    <!--Page Level JS-->
    <script src="../assets/plugins/countTo/jquery.countTo.js"></script>
    <script src="../assets/plugins/weather/js/skycons.js"></script>

     <script src="../assets/plugins/dataTables/js/jquery.dataTables.js"></script>
    <script src="../assets/plugins/dataTables/js/dataTables.bootstrap.js"></script>
    <!-- FlotCharts  -->
    <script src="../assets/plugins/flot/js/jquery.flot.min.js"></script>
    <script src="../assets/plugins/flot/js/jquery.flot.resize.min.js"></script>
    <script src="../assets/plugins/flot/js/jquery.flot.canvas.min.js"></script>
    <script src="../assets/plugins/flot/js/jquery.flot.image.min.js"></script>
    <script src="../assets/plugins/flot/js/jquery.flot.categories.min.js"></script>
    <script src="../assets/plugins/flot/js/jquery.flot.crosshair.min.js"></script>
    <script src="../assets/plugins/flot/js/jquery.flot.errorbars.min.js"></script>
    <script src="../assets/plugins/flot/js/jquery.flot.fillbetween.min.js"></script>
    <script src="../assets/plugins/flot/js/jquery.flot.navigate.min.js"></script>
    <script src="../assets/plugins/flot/js/jquery.flot.pie.min.js"></script>
    <script src="../assets/plugins/flot/js/jquery.flot.selection.min.js"></script>
    <script src="../assets/plugins/flot/js/jquery.flot.stack.min.js"></script>
    <script src="../assets/plugins/flot/js/jquery.flot.symbol.min.js"></script>
    <script src="../assets/plugins/flot/js/jquery.flot.threshold.min.js"></script>
    <script src="../assets/plugins/flot/js/jquery.colorhelpers.min.js"></script>
    <script src="../assets/plugins/flot/js/jquery.flot.time.min.js"></script>
    <script src="../assets/plugins/flot/js/jquery.flot.example.js"></script>
    <!-- Morris  -->
    <script src="../assets/plugins/morris/js/morris.min.js"></script>
    <script src="../assets/plugins/morris/js/raphael.2.1.0.min.js"></script>
    <!-- Vector Map  -->
    <script src="../assets/plugins/jvectormap/js/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="../assets/plugins/jvectormap/js/jquery-jvectormap-world-mill-en.js"></script>
    <!-- ToDo List  -->
    <script src="../assets/plugins/todo/js/todos.js"></script>
    <!--Load these page level functions-->
    <script src="../assets/plugins/sweet-alert/js/sweet-alert.min.js"></script>
    <script type="text/javascript" src="../assets/plugins/datepicker/js/bootstrap-datepicker.js"></script>
    <script>
    $(function(){
        $('.datepicker').datepicker();

    });

    </script>
    <script>
        $(document).ready(function() {
            $('#example').dataTable();
        });
    </script>
    <script>
        $(document).ready(function() {
            app.timer();
            app.map();
            app.weather();
            app.morrisPie();
        });
    </script>


    
          

</body>

</html>