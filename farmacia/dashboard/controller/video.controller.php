<?php
require_once 'model/video.php';

class videoController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new video();
    }
    
    public function Index(){
        require_once 'header.php';
        require_once 'view/video/index.php';
        require_once 'footer.php';
    }
    
    public function Crud(){
        $video = new video();
        
        if(isset($_REQUEST['id'])){
            $video = $this->model->Obtener($_REQUEST['id']);
        }
        
        require_once 'header.php';
        require_once 'view/video/crud-video.php';
        require_once 'footer.php';
    }
    
    public function Guardar(){
        $video = new video();       
 

        $video->idvideo  =$_REQUEST['txtcodvideo'];
        $video->titulo        =$_REQUEST['txtTitulo'];
        $video->url        =$_REQUEST['txtUrl'];
        $video->portafolio_idportafolio =$_REQUEST['cboPortafolio'];
        

        $video->idvideo > 0 
            ? $this->model->Actualizar($video)
            : $this->model->Registrar($video);
        
        header('Location: videoindex.php');
    }
    
    public function Eliminar(){
        


        $this->model->Eliminar($_REQUEST['id']);
        header('Location: videoindex.php');
    }

}