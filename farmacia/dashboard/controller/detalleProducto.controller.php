<?php
require_once 'model/detalleProducto.php';

class detalleProductoController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new detalleProducto();
    }
    
    public function Index(){
        require_once 'header.php';
        require_once 'view/detalleProducto/listar-detalleProducto.php';
        require_once 'footer.php';
    }
    
    public function Crud(){
        $detpro = new detalleProducto();
        
        if(isset($_REQUEST['id'])){
            $detpro = $this->model->Obtener($_REQUEST['id']);
        }
        
        require_once 'header.php';
        require_once 'view/detalleProducto/agregar-detalleProducto.php';
        require_once 'footer.php';
    }

    public function Crud2(){
        $detpro = new detalleProducto();
        
        if(isset($_REQUEST['id'])){
            $detpro = $this->model->Obtener2($_REQUEST['id']);
        }
        
        require_once 'header.php';
        require_once 'view/detalleProducto/editar-detalleProducto.php';
        require_once 'footer.php';
    }
    
    // para agregar
    public function Guardar(){
        $detpro = new detalleProducto();

        $imgFile    = $_FILES['txtFoto']['name'];
        $tmp_dir    = $_FILES['txtFoto']['tmp_name'];
        $imgSize    = $_FILES['txtFoto']['size'];
        $upload_dir = 'detalle-producto-images/'; // upload directory
    
        $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension    
        // valid image extensions
        $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions    
        // rename uploading image
        // $userpic = rand(1000,1000000).".".$imgExt;
        $userpic = rand(1000,1000000).".".$imgExt;
        // allow valid image file formats
        if(in_array($imgExt, $valid_extensions)){           
            // Check file size '1MB'
            if($imgSize < 1000000)              {
                move_uploaded_file($tmp_dir,$upload_dir.$userpic);
            }
            else{
                header("Location: detalleProductoindex.php");
            }
        }
        else{
            header("Location: detalleProductoindex.php");     
        }
        // SCRIPT PARA CARGAR IMAGEN DE PRECIO
        $imgFile2    = $_FILES['txtFoto2']['name'];
        $tmp_dir2    = $_FILES['txtFoto2']['tmp_name'];
        $imgSize2    = $_FILES['txtFoto2']['size'];
        $upload_dir2 = 'detalle-producto-images/'; // upload directory
    
        $imgExt2 = strtolower(pathinfo($imgFile2,PATHINFO_EXTENSION)); // get image extension    
        // valid image extensions
        $valid_extensions2 = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions    
        // renombramos el nombre la foto
        // $userpic2 = rand(1000,1000000).".".$imgExt2;
        $userpic2 = rand(1000,1000000).".".$imgExt2; 
        // allow valid image file formats
        if(in_array($imgExt2, $valid_extensions2)){           
            // Check file size '1MB'
            if($imgSize2 < 1000000){
                move_uploaded_file($tmp_dir2,$upload_dir2.$userpic2);
            }
            else{
                header("Location: detalleProductoindex.php");
            }
        }
        else{
            header("Location: detalleProductoindex.php");     
        }

        $detpro->nombre_subproducto   =$_REQUEST['txtNomProducto'];
        $detpro->caracteristicas    =$_REQUEST['txtDescripcion'];
        $detpro->precio   =$_REQUEST['txtPrecio'];
        $detpro->foto        =$userpic;
        $detpro->foto2        =$userpic2;
        $detpro->idproducto =$_REQUEST['txtCodProducto'];        
        $detpro->idusuario =$_REQUEST['cboUsuario'];   

        $detpro->iddetalleproducto > 0 
            ? $this->model->Actualizar($detpro)
            : $this->model->Registrar($detpro);
        
        header('Location: detalleProductoindex.php');
    }

    // para editar
    public function Guardar2(){
        $detpro = new detalleProducto();

        $imgFile    = $_FILES['txtFoto']['name'];
        $tmp_dir    = $_FILES['txtFoto']['tmp_name'];
        $imgSize    = $_FILES['txtFoto']['size'];
        $upload_dir = 'detalle-producto-images/'; // upload directory
    
        $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension    
        // valid image extensions
        $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions    
        // rename uploading image
        // $userpic = rand(1000,1000000).".".$imgExt;
        $userpic = rand(1000,1000000).".".$imgExt;
        // allow valid image file formats
        if(in_array($imgExt, $valid_extensions)){           
            // Check file size '1MB'
            if($imgSize < 1000000)              {
                move_uploaded_file($tmp_dir,$upload_dir.$userpic);
            }
            else{
                header("Location: detalleProductoindex.php");
            }
        }
        else{
            header("Location: detalleProductoindex.php");     
        }

         // SCRIPT PARA CARGAR IMAGEN DE PRECIO
        $imgFile2    = $_FILES['txtFoto2']['name'];
        $tmp_dir2    = $_FILES['txtFoto2']['tmp_name'];
        $imgSize2    = $_FILES['txtFoto2']['size'];
        $upload_dir2 = 'detalle-producto-images/'; // upload directory
    
        $imgExt2 = strtolower(pathinfo($imgFile2,PATHINFO_EXTENSION)); // get image extension    
        // valid image extensions
        $valid_extensions2 = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions    
        // renombramos el nombre la foto
        // $userpic2 = rand(1000,1000000).".".$imgExt2;
        $userpic2 = rand(1000,1000000).".".$imgExt2; 
        // allow valid image file formats
        if(in_array($imgExt2, $valid_extensions2)){           
            // Check file size '1MB'
            if($imgSize2 < 1000000){
                move_uploaded_file($tmp_dir2,$upload_dir2.$userpic2);
            }
            else{
                header("Location: detalleProductoindex.php");
            }
        }
        else{
            header("Location: detalleProductoindex.php");     
        }

        $detpro->iddetalleproducto =$_REQUEST['txtCodProducto'];
        $detpro->nombre_subproducto   =$_REQUEST['txtNomProducto'];
        $detpro->caracteristicas    =$_REQUEST['txtDescripcion'];
        $detpro->precio   =$_REQUEST['txtPrecio'];
        $detpro->foto        =$userpic;
        $detpro->foto2        =$userpic2;
        $detpro->idproducto =$_REQUEST['txtCodProducto'];
         $detpro->idusuario =$_REQUEST['cboUsuario'];         

        $detpro->iddetalleproducto > 0 
            ? $this->model->Actualizar($detpro)
            : $this->model->Registrar($detpro);
        
        header('Location: detalleProductoindex.php');
    }
    
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        header('Location: detalleProductoindex.php');
    }

}