<?php
require_once 'model/usos.php';

class usosController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new usos();
    }
    
    public function Index(){
        require_once 'header.php';
        require_once 'view/usos/index.php';
        require_once 'footer.php';
    }
    
    public function Crud(){
        $usos = new usos();
        
        if(isset($_REQUEST['id'])){
            $usos = $this->model->Obtener($_REQUEST['id']);
        }
        
        require_once 'header.php';
        require_once 'view/usos/crud-usos.php';
        require_once 'footer.php';
    }
    
    public function Guardar(){
        $usos = new usos();       

         // SCRIPT PARA CARGAR LOGO DE LA MARCA
        $imgFile    = $_FILES['txtFoto']['name'];
        $tmp_dir    = $_FILES['txtFoto']['tmp_name'];
        $imgSize    = $_FILES['txtFoto']['size'];
        $upload_dir = 'usos-images/'; // upload directory
    
        $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension    
        // valid image extensions
        $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions    
        // renombramos el nombre la foto
        // $userpic = rand(1000,1000000).".".$imgExt;
        $userpic = rand(1000,1000000).".".$imgExt; 
        // allow valid image file formats
        if(in_array($imgExt, $valid_extensions)){           
            // Check file size '1MB'
            if($imgSize < 1000000){
                move_uploaded_file($tmp_dir,$upload_dir.$userpic);
            }
            else{
                header("Location: portafolioindex.php");
            }
        }
        else{
            header("Location: portafolioindex.php");     
        }

        $usos->idusos  =$_REQUEST['txtcodUsos'];
        $usos->foto        =$userpic;
        $usos->portafolio_idportafolio =$_REQUEST['cboPortafolio'];
        

        $usos->idusos > 0 
            ? $this->model->Actualizar($usos)
            : $this->model->Registrar($usos);
        
        header('Location: usosindex.php');
    }
    
    public function Eliminar(){
        


        $this->model->Eliminar($_REQUEST['id']);
        header('Location: usosindex.php');
    }

}