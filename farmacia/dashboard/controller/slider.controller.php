<?php
require_once 'model/slider.php';

class sliderController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new slider();
    }
    
    public function Index(){
        require_once 'header.php';
        require_once 'view/slider/listar-slider.php';
        require_once 'footer.php';
    }
    
    public function Crud(){
        $slid = new slider();
        
        if(isset($_REQUEST['id'])){
            $slid = $this->model->Obtener($_REQUEST['id']);
        }
        
        require_once 'header.php';
        require_once 'view/slider/crud-slider.php';
        require_once 'footer.php';
    }
    
    public function Guardar(){
        $slid = new slider();

        $imgFile    = $_FILES['txtFoto']['name'];
        $tmp_dir    = $_FILES['txtFoto']['tmp_name'];
        $imgSize    = $_FILES['txtFoto']['size'];
        $upload_dir = 'slider-images/'; // upload directory
    
        $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension    
        // valid image extensions
        $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions    
        // renombramos el nombre la foto
        // $userpic = rand(1000,1000000).".".$imgExt;
        $userpic = rand(1000,1000000).".".$imgExt; 
        // allow valid image file formats
        if(in_array($imgExt, $valid_extensions)){           
            // Check file size '1MB'
            if($imgSize < 1000000){
                move_uploaded_file($tmp_dir,$upload_dir.$userpic);
            }
            else{
                header("Location: sliderindex.php");
            }
        }
        else{
            header("Location: sliderindex.php");     
        }


        
        $slid->idslider    =$_REQUEST['txtCodSlider'];
        // $slid->foto     =$_REQUEST['txtFoto'];
        $slid->foto        = $userpic;
        $slid->descripcion =$_REQUEST['txtDescripcion'];
        

        $slid->idslider > 0 
            ? $this->model->Actualizar($slid)
            : $this->model->Registrar($slid);
        
        header('Location: sliderindex.php');
    }
    
    public function Eliminar(){
        


        $this->model->Eliminar($_REQUEST['id']);
        header('Location: sliderindex.php');
    }

}