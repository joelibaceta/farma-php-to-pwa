<?php
require_once 'model/portafolio.php';

class portafolioController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new portafolio();
    }
    
    public function Index(){
        require_once 'header.php';
        require_once 'view/portafolio/listar-portafolio.php';
        require_once 'footer.php';
    }
    
    public function Crud(){
        $porta = new portafolio();
        
        if(isset($_REQUEST['id'])){
            $porta = $this->model->Obtener($_REQUEST['id']);
        }
        
        require_once 'header.php';
        require_once 'view/portafolio/crud-portafolio.php';
        require_once 'footer.php';
    }
    
    public function Guardar(){
        $porta = new portafolio();

        // SCRIPT PARA CARGAR LOGO DE LA MARCA
        $imgFile    = $_FILES['txtFoto']['name'];
        $tmp_dir    = $_FILES['txtFoto']['tmp_name'];
        $imgSize    = $_FILES['txtFoto']['size'];
        $upload_dir = 'portafolio-images/'; // upload directory
    
        $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension    
        // valid image extensions
        $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions    
        // renombramos el nombre la foto
        // $userpic = rand(1000,1000000).".".$imgExt;
        $userpic = rand(1000,1000000).".".$imgExt; 
        // allow valid image file formats
        if(in_array($imgExt, $valid_extensions)){           
            // Check file size '1MB'
            if($imgSize < 1000000){
                move_uploaded_file($tmp_dir,$upload_dir.$userpic);
            }
            else{
                header("Location: portafolioindex.php");
            }
        }
        else{
            header("Location: portafolioindex.php");     
        }

        // SCRIPT PARA CARGAR banner DE LA MARCA
        $imgFile2    = $_FILES['txtFoto2']['name'];
        $tmp_dir2    = $_FILES['txtFoto2']['tmp_name'];
        $imgSize2    = $_FILES['txtFoto2']['size'];
        $upload_dir2 = 'portafolio-images/'; // upload directory
    
        $imgExt2 = strtolower(pathinfo($imgFile2,PATHINFO_EXTENSION)); // get image extension    
        // valid image extensions
        $valid_extensions2 = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions    
        // renombramos el nombre la foto
        // $userpic2 = rand(1000,1000000).".".$imgExt2;
        $userpic2 = rand(1000,1000000).".".$imgExt2; 
        // allow valid image file formats
        if(in_array($imgExt2, $valid_extensions2)){           
            // Check file size '1MB'
            if($imgSize2 < 1000000){
                move_uploaded_file($tmp_dir2,$upload_dir2.$userpic2);
            }
            else{
                header("Location: portafolioindex.php");
            }
        }
        else{
            header("Location: portafolioindex.php");     
        }
         // SCRIPT PARA CARGAR banner DE LA MARCA
        $imgFile3    = $_FILES['txtFoto3']['name'];
        $tmp_dir3    = $_FILES['txtFoto3']['tmp_name'];
        $imgSize3    = $_FILES['txtFoto3']['size'];
        $upload_dir3 = 'portafolio-images/'; // upload directory
    
        $imgExt3 = strtolower(pathinfo($imgFile3,PATHINFO_EXTENSION)); // get image extension    
        // valid image extensions
        $valid_extensions3 = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions    
        // renombramos el nombre la foto
        // $userpic2 = rand(1000,1000000).".".$imgExt3;
        $userpic3 = rand(1000,1000000).".".$imgExt3; 
        // allow valid image file formats
        if(in_array($imgExt3, $valid_extensions3)){           
            // Check file size '1MB'
            if($imgSize3 < 1000000){
                move_uploaded_file($tmp_dir3,$upload_dir3.$userpic3);
            }
            else{
                header("Location: portafolioindex.php");
            }
        }
        else{
            header("Location: portafolioindex.php");     
        }


        
        $porta->idportafolio    =$_REQUEST['txtCodportafolio'];
        // $porta->foto     =$_REQUEST['txtFoto'];
        $porta->foto        = $userpic;
        $porta->foto2       = $userpic2;
        $porta->foto3       = $userpic3;
        $porta->descripcion =$_REQUEST['txtDescripcion'];
        $porta->idusuario   =$_REQUEST['cboUsuario'];
        

        $porta->idportafolio > 0 
            ? $this->model->Actualizar($porta)
            : $this->model->Registrar($porta);
        
        header('Location: portafolioindex.php');
    }
    
    public function Eliminar(){
        


        $this->model->Eliminar($_REQUEST['id']);
        header('Location: portafolioindex.php');
    }

}