<?php
require '../include/conexion.php';
page_protect();
?>

<!DOCTYPE html>
<html class="no-js">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>P&G</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" /> 
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="../assets/plugins/bootstrap/css/bootstrap.min.css">
    <!-- Fonts from Font Awsome -->
    <link rel="stylesheet" href="../assets/css/font-awesome.min.css">
    <!-- CSS Animate -->
    <link rel="stylesheet" href="../assets/css/animate.css">
    <!-- CSS personalizado -->
    <!-- <link rel="stylesheet" href="../css/personalizado.css"> -->
    <!-- Custom styles for this theme -->
    <link rel="stylesheet" href="../assets/css/main.css">
     <!-- DataTables-->
    <link rel="stylesheet" href="../assets/plugins/dataTables/css/dataTables.css">
    <!-- Vector Map  -->
    <link rel="stylesheet" href="../assets/plugins/jvectormap/css/jquery-jvectormap-1.2.2.css">
    <!-- ToDos  -->
    <link rel="stylesheet" href="../assets/plugins/todo/css/todos.css">
    <!-- Morris  -->
    <link rel="stylesheet" href="../assets/plugins/morris/css/morris.css">
    <!-- sweet alert -->
    <link rel="stylesheet" href="../assets/plugins/sweet-alert/css/sweet-alert.css">
    <!-- datepicker -->
    <link rel="stylesheet" href="../assets/plugins/datepicker/css/datepicker.css">
    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900,300italic,400italic,600italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <!-- Feature detection -->
    <script src="../assets/js/jquery-1.10.2.min.js"></script>

    <script src="../assets/js/modernizr-2.6.2.min.js"></script>
   
</head>

<body>
    <section id="container">
        <header id="header">
            <!--logo start-->
            <div class="brand">
                <a href="../dashboard" class="logo"><span>Farmacia</span></a>
            </div>
            <!--logo end-->
            <div class="toggle-navigation toggle-left">
                <button type="button" class="btn btn-default" id="toggle-left" data-toggle="tooltip" data-placement="right" title="Boton de Navegacion">
                    <i class="fa fa-bars"></i>
                </button>
            </div>
            <div class="user-nav">
                <ul>
                    <li class="dropdown messages">
                        <span class="badge badge-danager animated bounceIn" id="new-messages">5</span>
                        <button type="button" class="btn btn-default dropdown-toggle options" id="toggle-mail" data-toggle="dropdown">
                            <i class="fa fa-envelope"></i>
                        </button>
                        <ul class="dropdown-menu alert animated fadeInDown">
                            <li>
                                <h1>You have <strong>5</strong> new messages</h1>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="profile-photo">
                                        <img src="../assets/img/avatar.gif" alt="" class="img-circle">
                                    </div>
                                    <div class="message-info">
                                        <span class="sender">James Bagian</span>
                                        <span class="time">30 mins</span>
                                        <div class="message-content">Lorem ipsum dolor sit amet, elit rutrum felis sed erat augue fusce...</div>
                                    </div>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <div class="profile-photo">
                                        <img src="../assets/img/avatar1.gif" alt="" class="img-circle">
                                    </div>
                                    <div class="message-info">
                                        <span class="sender">Jeffrey Ashby</span>
                                        <span class="time">2 hour</span>
                                        <div class="message-content">hendrerit pellentesque, iure tincidunt, faucibus vitae dolor aliquam...</div>
                                    </div>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <div class="profile-photo">
                                        <img src="../assets/img/avatar2.gif" alt="" class="img-circle">
                                    </div>
                                    <div class="message-info">
                                        <span class="sender">John Douey</span>
                                        <span class="time">3 hours</span>
                                        <div class="message-content">Penatibus suspendisse sit pellentesque eu accumsan condimentum nec...</div>
                                    </div>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <div class="profile-photo">
                                        <img src="../assets/img/avatar3.gif" alt="" class="img-circle">
                                    </div>
                                    <div class="message-info">
                                        <span class="sender">Ellen Baker</span>
                                        <span class="time">7 hours</span>
                                        <div class="message-content">Sem dapibus in, orci bibendum faucibus tellus, justo arcu...</div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="profile-photo">
                                        <img src="../assets/img/avatar4.gif" alt="" class="img-circle">
                                    </div>
                                    <div class="message-info">
                                        <span class="sender">Ivan Bella</span>
                                        <span class="time">6 hours</span>
                                        <div class="message-content">Curabitur metus faucibus sapien elit, ante molestie sapien...</div>
                                    </div>
                                </a>
                            </li>
                            <li><a href="#">Check all messages <i class="fa fa-angle-right"></i></a>
                            </li>
                        </ul>

                    </li>
                    <li class="profile-photo">
                        <img src="../assets/img/avatar.png" alt="" class="img-circle">
                    </li>
                    <li class="dropdown settings">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color: #293949">
                      <?php echo $_SESSION['full_name']; ?>  <i class="fa fa-angle-down"></i>
                    </a>
                        <ul class="dropdown-menu animated fadeInDown">
                            <li>
                                <a href="#"><i class="fa fa-user"></i> Mi perfil</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-calendar"></i> Calendario   </a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-envelope"></i> Inbox <span class="badge badge-danager" id="user-inbox">5</span></a>
                            </li>
                            <li>
                                <a href="logout.php"><i class="fa fa-power-off"></i> Cerrar Sesion</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <div class="toggle-navigation toggle-right">
                            <button type="button" class="btn btn-default" id="toggle-right">
                                <i class="fa fa-comment"></i>
                            </button>
                        </div>
                    </li>

                </ul>
            </div>
        </header>
        <!--sidebar left start-->
        <aside class="sidebar">
            <div id="leftside-navigation" class="nano">
                <ul class="nano-content">
                    <li class="active">
                        <a href="../dashboard"><i class="fa fa-home"></i><span>Dashboard</span></a>
                    </li>
                    <li class="sub-menu">
                        <a href="javascript:void(0);"><i class="fa fa-picture-o" ></i><span>Sliders </span><i class="arrow fa fa-angle-right pull-right"></i></a>
                        <ul>

                            <li><a href="sliderindex.php?c=slider&a=Crud">Añadir nuevo slider</a></li>
                            <li><a href="sliderindex.php">Ver / Todos los slider</a></li>
                            
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="javascript:void(0);"><i class="fa fa-users"></i><span>Usuarios</span><i class="arrow fa fa-angle-right pull-right"></i></a>
                        <ul>
                            <li><a href="usuarioindex.php?c=usuario&a=Crud">Añadir nuevo Usuario</a></li>                             
                            <li><a href="usuarioindex.php">Ver / Todos los Usuarios</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="javascript:void(0);"><i class="fa fa-folder-open" ></i><span>Portafolio </span><i class="arrow fa fa-angle-right pull-right"></i></a>
                        <ul>

                            <li><a href="portafolioindex.php?c=portafolio&a=Crud">Añadir nuevo portafolio</a></li>
                            <li><a href="portafolioindex.php">Ver / Todos los portafolios</a></li>
                            
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="javascript:void(0);"><i class="fa fa-shopping-cart" aria-hidden="true"></i><span>Productos</span><i class="arrow fa fa-angle-right pull-right"></i></a>
                        <ul>
                            <li><a href="productosindex.php?c=productos&a=Crud">Añadir Productos</a></li>
                            <li><a href="productosindex.php">Ver Todos los Productos</a></li>
                            <li><a href="detalleProductoindex.php">Ver / Detalle de Productos</a></li>
                            
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="javascript:void(0);"><i class="fa fa-picture-o" ></i><span>Usos </span><i class="arrow fa fa-angle-right pull-right"></i></a>
                        <ul>

                            <li><a href="usosindex.php?c=usos&a=Crud">Añadir nuevo Uso</a></li>
                            <li><a href="usosindex.php">Ver / Todos los Usos</a></li>
                            
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="javascript:void(0);"><i class="fa fa-picture-o" ></i><span>Videos </span><i class="arrow fa fa-angle-right pull-right"></i></a>
                        <ul>

                            <li><a href="videoindex.php?c=video&a=Crud">Añadir nuevo video</a></li>
                            <li><a href="videoindex.php">Ver / Todos los video</a></li>
                            
                        </ul>
                    </li>
                    
                    
                </ul>
            </div>

        </aside>
        <!--sidebar left end-->
<!--main content start-->
<section class="main-content-wrapper">
    <section id="main-content">


