/*
drop database if exists farmacia;
create database farmacia;
use farmacia; */

use jun25cad_farmacia;

drop table if exists tbl_perfiles;
create table tbl_perfiles(
idperfil int auto_increment primary key not null,
nomperfil varchar(100) not null

);
insert into tbl_perfiles(nomperfil) values('administrador');
insert into tbl_perfiles(nomperfil) values('vendedor');


drop table if exists tbl_usuario;
create table tbl_usuario(
idusuario int auto_increment primary key not  null,
dni varchar(100)  null,
nomusu varchar(100)  null,
apeusu varchar(100)  null,
corrusu varchar(100)  null,
clausu varchar(100)  null,
perfil_idperfil int  null
);
insert into tbl_usuario(dni,nomusu,apeusu,corrusu,clausu,perfil_idperfil) values('87654321','admin','admin','admin@hotmail.com','123',1);

/*
SELECT idusuario,dni,nomusu,apeusu,corrusu,clausu,p.idperfil,p.nomperfil
from tbl_usuario u 
inner join tbl_perfiles p
on u.perfil_idperfil=p.idperfil
where idusuario=1;

select * from tbl_usuario;*/

drop table if exists tbl_portafolio;
create table tbl_portafolio(
idportafolio int auto_increment primary key not  null,
foto varchar(100)not null,
foto2 varchar(100)not null,
foto3 varchar(100)not null,
descripcion varchar(100)not null,
idusuario int not null,

foreign key (idusuario) references tbl_usuario(idusuario)
);
 


drop table if exists tbl_usos;
create table tbl_usos(
idusos int auto_increment primary key not  null,
foto varchar(100)not null,
portafolio_idportafolio int not null,

 foreign key (portafolio_idportafolio) references tbl_portafolio(idportafolio)
);

/*
SELECT * FROM tbl_usos;

SELECT po.idportafolio, descripcion,idusos, u.foto as fotousos
from tbl_portafolio po
left outer join tbl_usos u
on u.portafolio_idportafolio=po.idportafolio

				where idusos = 1;

*/

drop table if exists tbl_video;
create table tbl_video(
idvideo int auto_increment primary key not  null,
titulo varchar(100)not null,
url varchar(100)not null,
portafolio_idportafolio int not null,

foreign key (portafolio_idportafolio) references tbl_portafolio(idportafolio)
);
/*

SELECT * FROM tbl_video;

SELECT po.idportafolio, descripcion,idvideo,url
from tbl_portafolio po
inner join tbl_video u
on u.portafolio_idportafolio=po.idportafolio

				where idvideo =1;

SELECT po.idportafolio,idvideo,url,portafolio_idportafolio
from tbl_portafolio po
inner join tbl_video u
on u.portafolio_idportafolio=po.idportafolio

				where po.idportafolio =1
;

SELECT po.idportafolio,po.foto as fotoporta,foto2, foto3, descripcion, descripcion, idusuario,idusos, u.foto as fotousos
,idvideo,url
from tbl_portafolio po
inner join tbl_usos u
on u.portafolio_idportafolio=po.idportafolio
inner join tbl_video v
on v.portafolio_idportafolio=po.idportafolio;
*/


drop table if exists tbl_productos;
create table tbl_productos(
idproducto int auto_increment primary key not  null,
nombre varchar(100)not null,
color_nombre varchar(100)not null,
subtitulo varchar(100)not null,
descripcion varchar(500)not null,
foto varchar(100)not null,
foto2 varchar(100)not null,
fecharegistro date,
idportafolio int not null,


foreign key (idportafolio) references tbl_portafolio(idportafolio)

);
/*
SELECT * FROM tbl_productos;

SELECT idproducto,nombre,color_nombre,subtitulo,p.foto,p.foto2,fecharegistro,pf.idportafolio,pf.descripcion as marca
from tbl_productos p
inner join tbl_portafolio pf
on pf.idportafolio=p.idportafolio;

SELECT idproducto,nombre,p.foto as fotopro,fecharegistro,pf.idportafolio as idporta,pf.descripcion
				from tbl_productos p
				inner join tbl_portafolio pf
				on pf.idportafolio=p.idportafolio  

				where p.idportafolio =1;


SELECT idproducto,nombre,p.descripcion,precio,p.foto,fecharegistro,u.idusuario,nomusu,dni,pf.idportafolio,pf.descripcion,pf.foto as fotoporta
from tbl_productos p
inner join tbl_usuario u
on u.idusuario=p.idusuario
inner join tbl_portafolio pf
on pf.idportafolio=p.idportafolio

where u.idusuario = 2 and pf.idportafolio =1;

*/

drop table if exists tbl_detalleProducto;
create table tbl_detalleProducto(
iddetalleproducto int auto_increment primary key not  null,
nombre_subproducto varchar(100)not null,
caracteristicas varchar(600)not null,
precio double(7,2),
-- color_fondo_precio varchar(100)not null,
foto varchar(100)not null,
foto2 varchar(100)not null,
idproducto int not null,
idusuario int not null,
 
foreign key (idproducto) references tbl_productos(idproducto),

 
foreign key (idusuario) references tbl_usuario(idusuario)
);
/*
SELECT p.idportafolio,dp.idproducto,nombre,dp.iddetalleproducto,nombre_subproducto,caracteristicas,precio,dp.foto as fotodetallepro,po.foto3,dp.foto2
            from tbl_detalleProducto dp
            inner join tbl_productos p
            on p.idproducto=dp.idproducto
            inner join tbl_portafolio po
            on p.idportafolio=po.idportafolio
            inner join tbl_usuario u 
            on u.idusuario=dp.idusuario
            where dp.idproducto = 1 and  u.idusuario = 2;
            
            select *  from tbl_productos;
            
SELECT idproducto,nombre,p.foto as fotopro,pf.foto3 as fototextura
    from tbl_productos p
    inner join tbl_portafolio pf
    on pf.idportafolio=p.idportafolio

    where pf.idportafolio = 1;           


select *  from tbl_productos;

SELECT * from tbl_detalleProducto;

SELECT  p.idproducto as idproducto,p.nombre,dp.iddetalleproducto,caracteristicas,precio,dp.foto as foto,usu.idusuario,usu.nomusu,usu.apeusu
from tbl_detalleProducto dp
left outer join tbl_productos p
on p.idproducto=dp.idproducto
left outer join tbl_usuario usu
on dp.idusuario=usu.idusuario  where iddetalleproducto = 1;


SELECT dp.idproducto,p.nombre,dp.iddetalleproducto,nombre_subproducto,caracteristicas,precio,dp.foto
from tbl_detalleProducto dp
left outer join tbl_productos p
on p.idproducto=dp.idproducto
where dp.idproducto =1;

SELECT dp.idproducto,nombre,dp.iddetalleproducto,nombre_subproducto,caracteristicas,precio,dp.foto as fotodetallepro,po.foto3
            from tbl_detalleProducto dp
            inner join tbl_productos p
            on p.idproducto=dp.idproducto
			inner join tbl_portafolio po
            on p.idportafolio=po.idportafolio
            inner join tbl_usuario u 
            on u.idusuario=dp.idusuario
            where dp.idproducto = 1 and  u.idusuario = 2  ;

SELECT p.idproducto as idproducto,p.nombre,dp.iddetalleproducto,caracteristicas,precio,dp.foto as foto,u.idusuario
from tbl_detalleProducto dp
inner join tbl_productos p
on p.idproducto=dp.idproducto
inner join tbl_usuario u
on u.idusuario=dp.idusuario; */


drop table if exists tbl_slider;
create table tbl_slider(
idslider int auto_increment primary key not  null,
foto varchar(100)not null,
descripcion varchar(100)not null

);

/*
SELECT * FROM tbl_slider;

SELECT count(*) as totalportafolio from tbl_portafolio;

 




SELECT distinct(pf.idportafolio) as id ,pf.foto as fotoporta ,pf.descripcion as nombreporta FROM tbl_portafolio pf
            inner join tbl_productos p
            on pf.idportafolio=p.idportafolio
            inner join tbl_usuario u
            on u.idusuario=p.idusuario

            where u.idusuario = 2 ;*/
 
