<?php
require '../include/conexion.php'; 
header('Content-Type: text/html; charset=ISO-8859-1');
page_protect()
?>

<!DOCTYPE html>
<html manifest="vendedores.manifest">
<!-- <html manifest="/vendedores.manifest"> -->
<head>
    <title>Ventas Promoción Farmacia</title>    
    <link rel="manifest" href="manifest.json">
    <meta http-equiv="Content-Type" content="text/html; charset=gb18030">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="../assets/js/jquery.3.3.1.min.js"></script> 
    <script src="bundle/app.js"></script>

    <link rel="stylesheet" href="../assets/css/bootstrap.css">
    <link rel="stylesheet" href="bundle/styles.css">
    
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

</head>
<body>
    <header id="header" class="cabecera-bill">
        <!--logo start-->
        <div class="brand brand-bill" >
            <a href="javascript:reload()" class="logo logopg"></a>
        </div>
        <div class="user-nav">
            <ul>
                <li class="dropdown settings combo-bill">
                    <a class="dropdown-toggle btn-bill" data-toggle="dropdown" href="#"  >
                        <i class="fa fa-bars"></i>
                    </a>
                    <ul class="dropdown-menu animated fadeInDown">
                        <li>
                            <a href="#"><i class="fa fa-user"></i> <?php echo $_SESSION['user_data']; ?></a>
                        </li>
                        <li>
                            <a href="trivial.php"><i class="fa fa-gamepad" aria-hidden="true"></i> Trivia</a>
                        </li>
                        <li>
                            <a href="Memoria.php"><i class="fa fa-gamepad" aria-hidden="true"></i> Memory</a>
                        </li>
                        <li>
                            <a href="logout.php"><i class="fa fa-power-off"></i> Cerrar Sesion</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </header>