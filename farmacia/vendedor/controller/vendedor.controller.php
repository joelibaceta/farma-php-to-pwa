<?php
require_once 'model/vendedor.php'; 

class vendedorController{
    
    private $model;
    
    function __CONSTRUCT(){
        $this->model = new vendedor();
    }

    public function ExecQuery( $query ) {
        $host     = "localhost"; // Host name 
        $username = "jun25cad_farma"; // Mysql username 
        $password = "farmacia2018"; // Mysql password 
        $db_name  = "jun25cad_farmacia"; // Database name

        // Connect to server and select databse.
        $con = mysqli_connect($host, $username, $password, $db_name);

        $results = array();
        $result = mysqli_query($con, $query);

        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
            $results[] = $row;
        }
        return $results;
    }

    public function GetPortfolios( $user_id ) {
        $query_portfolios = "SELECT po.idportafolio, po.foto AS fotoporta, foto2, foto3, descripcion, descripcion, idusuario 
                                FROM tbl_portafolio AS po";
        return $this->ExecQuery($query_portfolios);
    }

    public function GetSliders() {
        $query_sliders = "SELECT * from tbl_slider";
        return $this->ExecQuery($query_sliders);
    }

    public function GetUses() {
        $query_sliders = "SELECT * from tbl_usos";
        return $this->ExecQuery($query_sliders);
    }

    public function GetVideos() {
        $query_sliders = "SELECT * from tbl_video";
        return $this->ExecQuery($query_sliders);
    }

    public function GetProducts( $user_id ) {
        $query_products = " SELECT DISTINCT p.idproducto, nombre, color_nombre, subtitulo, 
                            p.descripcion, fecharegistro, 
                            p.foto AS fotopro, pf.foto3 AS fototextura, p.foto2,
                            p.idportafolio
                                FROM tbl_productos p
                                LEFT JOIN tbl_portafolio pf 
                                    ON pf.idportafolio = p.idportafolio 
                                WHERE pf.idusuario = " . $user_id;
        return $this->ExecQuery($query_products);
    }

    public function GetDetails( $user_id ) {
        $query_details = " SELECT * FROM tbl_detalleProducto WHERE idusuario = " . $user_id;
        return $this->ExecQuery($query_details);
    }
    
    public function Index(){
        require_once 'header.php'; 

        $user_id = $_SESSION['full_user'];

        $products = $this->GetProducts( $user_id );
        $portfolios = $this->GetPortfolios( $user_id );
        $details = $this->GetDetails( $user_id );
        $sliders = $this->GetSliders();
        $uses = $this->GetUses();
        $videos = $this->GetVideos();

        require_once 'view/vendedor/index.php';
        require_once 'footer.php';
    }
    
    public function Crud(){
        $slid = new vendedor();
        
        if(isset($_REQUEST['id'])){
            $slid = $this->model->Obtener($_REQUEST['id']);
        }
        
        require_once 'header.php';
        require_once 'view/vendedor/portafolio-productos.php';
        require_once 'footer.php';
    }
    
    public function Guardar(){
        $slid = new vendedor();       

        
        $slid->idvendedor  =$_REQUEST['txtCodvendedor'];
        $slid->foto        =$_REQUEST['txtFoto'];
        $slid->descripcion =$_REQUEST['txtDescripcion'];
        

        $slid->idvendedor > 0 
            ? $this->model->Actualizar($slid)
            : $this->model->Registrar($slid);
        
        header('Location: index.php');
    }
    
    public function Eliminar(){ 
        $this->model->Eliminar($_REQUEST['id']);
        header('Location: index.php');
    }

}