<?php
require_once 'model/usos.php';

class usosController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new usos();
    }
    
    public function Index(){
        require_once 'header.php';
        require_once 'view/usos/index.php';
        require_once 'footer.php';
    }
    
    public function Crud(){
        $slid = new usos();
        
        if(isset($_REQUEST['id'])){
            $slid = $this->model->Obtener($_REQUEST['id']);
        }
        
        require_once 'header.php';
        require_once 'view/usos/index.php';
        require_once 'footer.php';
    }
    
    public function Guardar(){
        $slid = new usos();       

        
        $slid->idusos  =$_REQUEST['txtCodusos'];
        $slid->foto        =$_REQUEST['txtFoto'];
        $slid->descripcion =$_REQUEST['txtDescripcion'];
        

        $slid->idusos > 0 
            ? $this->model->Actualizar($slid)
            : $this->model->Registrar($slid);
        
        header('Location: usosindex.php');
    }
    
    public function Eliminar(){
        


        $this->model->Eliminar($_REQUEST['id']);
        header('Location: usosindex.php');
    }

}