<?php
require_once 'model/detalle.php';

class detalleController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new detalle();
    }
    
    public function Index(){
        require_once 'header.php';
        require_once 'view/vendedor/listar-detalle.php';
        require_once 'footer.php';
    }
    
    public function Crud(){
        $pro = new detalle();
        
        if(isset($_REQUEST['id'])){
            $pro = $this->model->Obtener($_REQUEST['id']);
        }
        
        require_once 'header.php';
        require_once 'view/vendedor/detalle.php';
        require_once 'footer.php';
    }
    
    public function Guardar(){
        $pro = new detalle();

        $imgFile    = $_FILES['txtFoto']['name'];
        $tmp_dir    = $_FILES['txtFoto']['tmp_name'];
        $imgSize    = $_FILES['txtFoto']['size'];
        $upload_dir = 'producto-images/'; // upload directory
    
        $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension    
        // valid image extensions
        $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions    
        // rename uploading image
        // $userpic = rand(1000,1000000).".".$imgExt;
        $userpic = "product ".$pro->nombre = $_REQUEST['txtNombre'].".".$imgExt;
        // allow valid image file formats
        if(in_array($imgExt, $valid_extensions)){           
            // Check file size '1MB'
            if($imgSize < 1000000)              {
                move_uploaded_file($tmp_dir,$upload_dir.$userpic);
            }
            else{
                header("Location: detalleindex.php");
            }
        }
        else{
            header("Location: detalleindex.php");     
        }
        
        $pro->idproducto    =$_REQUEST['txtCodProducto'];
        $pro->nombre        =$_REQUEST['txtNombre'];
        $pro->descripcion   =$_REQUEST['txtDescripcion'];
        $pro->precio        =$_REQUEST['txtPrecio'];
        $pro->foto          =$userpic;
        $pro->fecharegistro =date('Y-m-d');
        $pro->idusuario     =$_REQUEST['cboUsuario'];
        $pro->idportafolio     =$_REQUEST['cboPortafolio'];
        

        $pro->idproducto > 0 
            ? $this->model->Actualizar($pro)
            : $this->model->Registrar($pro);
        
        header('Location: detalleindex.php');
    }
    
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        header('Location: detalleindex.php');
    }

}