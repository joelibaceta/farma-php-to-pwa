<?php

require_once 'model/database.php';
require_once 'update-manifest.php';

$controller = 'vendedor';


// Todo esta lógica hara el papel de un FrontController
 
if(!isset($_REQUEST['u']))
{
    require_once "controller/$controller.controller.php";
    $controller = ucwords($controller) . 'Controller';
    $controller = new $controller;
    $controller->Index();
} else {
    $manifest_builder = new ManifestBuilder();
    $manifest_builder->update_manifest();
}
// }
// else
// {
//     // Obtenemos el controlador que queremos cargar
//     $controller = strtolower($_REQUEST['c']);
//     $accion = isset($_REQUEST['a']) ? $_REQUEST['a'] : 'Index';
    
//     // Instanciamos el controlador
//     require_once "controller/$controller.controller.php";
//     $controller = ucwords($controller) . 'Controller';
//     $controller = new $controller;
    
//     // Llama la accion
//     call_user_func( array( $controller, $accion ) );
// }