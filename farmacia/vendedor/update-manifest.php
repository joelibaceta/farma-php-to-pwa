<?php

    class ManifestBuilder {

        function getDirContents($dir, &$results = array()){
            $fileinfos = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($dir)
            );
            foreach($fileinfos as $pathname => $fileinfo) {
                if (!$fileinfo->isFile()) continue;
                $results[] =  $pathname;
            }
        
            return $results;
        }

        public function update_manifest() {

            $manifestfile = fopen("vendedores.manifest", "w");

            fwrite($manifestfile, "CACHE MANIFEST \n\n");
            fwrite($manifestfile, "# UUID: " . uniqid()  . " \n\n");
            fwrite($manifestfile, "CACHE: \n");

            fwrite($manifestfile, "www.catalogohaircare.com/farmacia/vendedor/ \n");
            fwrite($manifestfile, "www.catalogohaircare.com/farmacia/vendedor/bundle/app.js \n");
            fwrite($manifestfile, "www.catalogohaircare.com/farmacia/assets/js/jquery.3.3.1.min.js \n");

            $detalle_producto_images_files = $this->getDirContents('../dashboard/portafolio-images');
            $portafolio_images_files = $this->getDirContents('../dashboard/portafolio-images');
            $producto_images_files = $this->getDirContents('../dashboard/portafolio-images');
            $slider_images_files = $this->getDirContents('../dashboard/portafolio-images');
            $usos_images_files = $this->getDirContents('../dashboard/portafolio-images');
            $videos = $this->getDirContents('../videos');

            fwrite($manifestfile, implode ("\n", $detalle_producto_images_files));
            fwrite($manifestfile, "\n");
            fwrite($manifestfile, implode ("\n", $portafolio_images_files));
            fwrite($manifestfile, "\n");
            fwrite($manifestfile, implode ("\n", $producto_images_files));
            fwrite($manifestfile, "\n");
            fwrite($manifestfile, implode ("\n", $slider_images_files));
            fwrite($manifestfile, "\n");
            fwrite($manifestfile, implode ("\n", $usos_images_files));
            fwrite($manifestfile, "\n");
            fwrite($manifestfile, implode ("\n", $videos));
            fwrite($manifestfile, "\n");

            echo "CACHE HAS BEEN UPDATED";

        }



    }


    
?>