'use strict';

var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var del = require('del');
var gulp = require('gulp');
var htmlmin = require('gulp-htmlmin');
var runSequence = require('run-sequence'); 
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');

// Set the browser that you want to support
const AUTOPREFIXER_BROWSERS = [
    'ie >= 10',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4.4',
    'bb >= 10'
  ];

gulp.task('styles', function () {
    return gulp.src(['./css/*.css']) //'../assets/css/*.css', 
        .pipe(concat('styles.css'))
        // Auto-prefix css styles for cross browser compatibility
        .pipe(autoprefixer({browsers: AUTOPREFIXER_BROWSERS}))
        // Minify the file
        .pipe(csso())
        // Output
        .pipe(gulp.dest('./bundle'))
  });

  // Gulp task to minify JavaScript files
gulp.task('scripts', function() {
    return gulp.src(['./js/*.js'])
        .pipe(concat('app.js'))
        // Minify the file
        .pipe(uglify())
        // Output
        .pipe(gulp.dest('./bundle'))
  });

// Clean output directory
gulp.task('clean', () => del(['bundle']));

// Gulp task to minify all files
gulp.task('default', ['clean'], function () {
    runSequence(
      'styles',
      'scripts'
    );
  });