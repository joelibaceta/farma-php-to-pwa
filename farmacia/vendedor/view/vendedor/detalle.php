
<?php
    $id_producto = $pro->idproducto;
    echo $id_producto;
?>
<br>
<br>
<div>
    <div class="section-fluido-producto " style="background-image: url('../dashboard/producto-images/<?php echo $pro->foto2; ?>');">
        <div class="col-sm-6 col-sm-offset-6 col-xs-12  cont">
            <div class="detalles-producto">
                <h1 class="nombre-pro" style="color: <?php echo $pro->color_nombre; ?>;"><?php echo utf8_decode($pro->nombre); ?> </h1>
                <div class="separador"></div>
                <p class="subtitulo"><?php echo utf8_decode($pro->subtitulo); ?> </p>  
                <h2 class="titulo2"> <strong class="t1">Beneficio <br></strong>      <strong class="t3">para tu  cabello</strong>  </h2>
                <p class="descripcion-prod"><?php echo utf8_decode($pro->descripcion); ?></p>
            </div>
        </div>
    </div>        
</div>

<?php
$id_usuario = $_SESSION['full_user'];
$query = 
        "SELECT p.idportafolio,dp.idproducto,nombre,dp.iddetalleproducto,nombre_subproducto,caracteristicas,precio,dp.foto as fotodetallepro,po.foto3,dp.foto2
        from tbl_detalleProducto dp
        inner join tbl_productos p on p.idproducto=dp.idproducto
        inner join tbl_portafolio po on p.idportafolio=po.idportafolio
        inner join tbl_usuario u on u.idusuario=dp.idusuario
        where dp.idproducto = $id_producto and  u.idusuario = $id_usuario";

//echo $query;
$result = mysqli_query($con, $query);
if (mysqli_affected_rows($con) != 0) {
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $id_producto = $row['idproducto'];
        $nombre = $row['nombre_subproducto'];
        $foto = $row['fotodetallepro'];
        $foto3 = $row['foto3'];
        $foto2 = $row['foto2'];
        $caracteristicas = $row['caracteristicas'];
        $precio = $row['precio'];
        $idPortafolio_bd = $row['idportafolio'];
        ?>
  
        <div class="textura" style="background-image: url('../dashboard/portafolio-images/<?php echo $foto3; ?>');">
        <div class="container detalle_prod">

            <div class="row" style="margin-bottom: 3em;">
                <div class="col-sm-6">
                    <img src="../dashboard/detalle-producto-images/<?php echo $foto; ?> " alt="" class="img-responsive img-products">
                </div>
                <div class="col-sm-6 " >
                    <div class="price_prod" > 
                        <strong class="price" style="background-image: url('../dashboard/detalle-producto-images/<?php echo $foto2; ?> ')">S/. <?php echo $precio; ?></strong>
                    </div> 

                    <div class="contenido-descripcion">
                        <h2><?php echo $nombre; ?></h2>
                        <p><?php echo $caracteristicas; ?></p>
                    </div>  
                </div>
            </div>
        </div>

    <?php
        }
    }
    ?>
 </div>   
    
     
<?php $id_portafolio = $idPortafolio_bd; ?>

<div class="row productos-flotantes"  >

    <?php 
        $query = "SELECT idproducto,nombre,p.foto as fotopro,pf.foto3 as fototextura
            from tbl_productos p
            inner join tbl_portafolio pf on pf.idportafolio=p.idportafolio
            where pf.idportafolio = $id_portafolio";

    //echo $query;
    $result = mysqli_query($con, $query);   
    $cant=0;
    if (mysqli_affected_rows($con) != 0) {
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $cant=$cant+1;
            $id_producto = $row['idproducto']; 
            $foto = $row['fotopro']; 
            $foto3 = $row['fototextura']; 
            $nombre = $row['nombre'];              
      ?>
        <div class="col-sm-2 " style="padding: 0px;">
        
            <div class="capa-img">
                <a href="?c=detalle&a=Crud&id=<?php echo $id_producto; ?>"><img src="../dashboard/producto-images/<?php echo $foto; ?>" class="img-responsive img-producto2"></a>
            </div>
            <!-- <h4 class="text-center"><?php echo $nombre; ?> </h4> -->
        </div> 
      
                
<?php 
        }
    }
?>  
    
</div>

<!-- </div> -->
 <input type="hidden"  id="txtCantidad" value="<?php echo $cant; ?>" >
<script > 
    var cant = $("#txtCantidad").val();
    // var x=screen.width;
    // var y=screen.height;
    var x=$( window ).width();
    var y=$( window ).height();
    

        var w=y/cant;
         $(".col-sm-2").css({
            "width": w,
        });
        
        var w1=x/cant;
         $(".col-sm-2").css({
            "width": w1,
        });
    
    if(x == 768 ){
        var w=x/cant;
         $(".col-sm-2").css({
            "width": w,
        });
    }else{
        if( y == 1024){
            var w=y/cant;
            
         $(".col-sm-2").css({
            "width": w,
        });
        }
    }
    

</script>