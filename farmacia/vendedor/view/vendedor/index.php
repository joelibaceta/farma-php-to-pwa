
<div class="all-container"> 
    <div class="main-page">
        <div class=" w3-display-container bg-section"  >
            <div class="swiper-container"> 
                <div class="swiper-wrapper"> 
                    <?php foreach ($sliders as $row ) { ?>
                        <div class="swiper-slide"
                            style="background-image: url('../dashboard/slider-images/<?php echo $row["foto"]; ?>');">  
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="portfolios-list">
            <?php foreach ($portfolios as $row ) { ?>
                <?php $port_id = $row['idportafolio']; ?>
                    <div class="row section-fluido row-portfolio"
                        style="background-image: url('../dashboard/portafolio-images/<?php echo $row["fotoporta"]; ?>');">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-sm-offset-6  text-center"> 
                            <a href="javascript:view_products('<?php echo $port_id ?>')" class="btn">
                                <img src="img/sku.png" alt="" class="img-responsive img-btn">
                            </a>
                            <a href="javascript:view_uses('<?php echo $port_id ?>')" class="btn">
                                <img src="img/uso.png" alt="" class="img-responsive img-btn">
                            </a>
                            <a href="javascript:view_video('<?php echo $port_id ?>')" class="btn">
                                <img src="img/play.png" alt="" class="img-responsive img-btn">
                            </a>
                        </div>
                    </div>
            <?php } ?>
        </div>
    </div>

    <div class="products-list">
        <?php foreach ($portfolios as $row ) { ?>
            <section class="imgen-principal">
                <div class="capa-image" data-portfolio-id="<?php echo $row['idportafolio']; ?>">
                    <img src="../dashboard/portafolio-images/<?php echo $row['foto2'] ?>" class="full-width-image" >
                </div>
            </section>
        <?php } ?>

        <?php $counter=0; ?>
        <div class="float-products"> 
            <?php foreach ($products as $row ) { ?>
                <div class="capa-img" data-portfolio-id="<?php echo $row['idportafolio']; ?>">
                    <a href="javascript:view_product_detail('<?php echo $row['idproducto']; ?>', '<?php echo $row['idportafolio']; ?>')">
                        <img src="../dashboard/producto-images/<?php echo $row['fotopro']; ?>" class="img-responsive img-producto2">
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>

    <div class="products-detail">
        <?php foreach ($products as $row ) { ?>
            <div class="product-detail" data-product-id="<?php echo $row['idproducto']; ?>">
                <div class="section-fluido-producto " style="background-image: url('../dashboard/producto-images/<?php echo $row['foto2']; ?>');">            
                    <div class="col-sm-6 col-sm-offset-6 col-xs-12  cont">
                        <div class="detalles-producto">
                            <h1 class="nombre-pro" style="color: <?php echo $row['color_nombre']; ?>;"><?php echo $row['nombre']; ?></h1>
                            <div class="separador"></div>
                            <p class="subtitulo"><?php echo $row['subtitulo']; ?> </p>   
                            <h2 class="titulo2"> <strong class="t1">Beneficio <br></strong> <strong class="t3">para tu  cabello</strong>  </h2>
                            <p class="descripcion-prod"><?php echo $row['descripcion']; ?></p>
                        </div>
                    </div>
                </div>        
            </div> 
        <?php } ?> 

        <?php foreach ($details as $row ) { ?>
            <div class="container detalle_prod" data-product-id="<?php echo $row['idproducto']; ?>">
                <div class="row" style="margin-bottom: 3em;">
                    <div class="col-sm-6">
                        <img src="../dashboard/detalle-producto-images/<?php echo $row['foto']; ?>" class="img-responsive img-products">
                    </div>
                    <div class="col-sm-6 " >
                        <div class="price_prod" > 
                            <strong class="price" style="background-image: url('../dashboard/detalle-producto-images/<?php echo $row['foto2']; ?>')">
                                S/. <?php echo $row['precio']; ?>
                            </strong>
                        </div>
                        <div class="contenido-descripcion">
                            <h2><?php echo $row['nombre_subproducto']; ?></h2>
                            <p><?php echo $row['caracteristicas']; ?></p>
                        </div>  
                    </div>
                </div> 
            </div>
        <?php } ?>
        <div class="float-products"> 
            <?php foreach ($products as $row ) { ?>
                <div class="capa-img" data-portfolio-id="<?php echo $row['idportafolio']; ?>">
                    <a href="javascript:view_product_detail('<?php echo $row['idproducto']; ?>')">
                        <img src="../dashboard/producto-images/<?php echo $row['fotopro']; ?>" class="img-responsive img-producto2">
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>

    <div class="uses">
        <?php foreach ($uses as $row ) { ?>
            <div class="imgen-contenido" data-portfolio-id="<?php echo $row['portafolio_idportafolio']; ?>">
                <img src="../dashboard/usos-images/<?php echo $row['foto']; ?>" alt="" class="img-responsive">
            </div>
        <?php } ?>
    </div>

    <div class="videos">
        <?php foreach ($videos as $row ) { ?> 
            <section class="contenedor-video" data-portfolio-id="<?php echo $row['portafolio_idportafolio']; ?>">
                <div class="container">
                    <div class="col-sm-12">
                        <div class="video-padre text-center">
                            <h2><?php echo $row['titulo']; ?></h2>
                            <video controls="" width="80%">
                                <source src="<?php echo $row['url']; ?>" type="video/ogg">
                                <source src="<?php echo $row['url']; ?>" type="video/mp4">
                                Tu navegador no implementa el elemento <code>video</code>.
                            </video>
                        </div>
                    </div>
                </div>
            </section> 
        <?php } ?>
    </div>
</div>



<script>
    
    $(document).ready(function () {
        //initialize swiper when document ready
        var mySwiper = new Swiper ('.swiper-container', {
        // Optional parameters
        direction: 'horizontal',
        loop: true
        })
    });

 
</script>