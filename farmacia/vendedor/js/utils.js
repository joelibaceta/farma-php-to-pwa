function reload() {
    document.querySelector(".main-page").style.display = "block";
    document.querySelector(".products-list").style.display = "none";
    document.querySelector(".products-detail").style.display = "none";
    document.querySelector(".uses").style.display = "none";
    document.querySelector(".videos").style.display = "none";
}

function view_products( portfolio_id ) {
    document.querySelector(".main-page").style.display = "none";
    filter_elements(".products-list .capa-image", "portfolio-id", portfolio_id);
    filter_elements(".float-products .capa-img", "portfolio-id", portfolio_id);
    document.querySelector(".products-list").style.display = "block"; 
    
}

function view_uses( portfolio_id ) {
    document.querySelector(".main-page").style.display = "none";
    filter_elements(".uses .imgen-contenido", "portfolio-id", portfolio_id); 
    document.querySelector(".uses").style.display = "block"; 
}

function view_product_detail( product_id, portfolio_id ) {
    document.querySelector(".products-list").style.display = "none";
    filter_elements(".products-detail .product-detail", "product-id", product_id);
    filter_elements(".products-detail .detalle_prod", "product-id", product_id);
    filter_elements(".products-detail .capa-imgcapa-img", "portfolio-id", portfolio_id);
    document.querySelector(".products-detail").style.display = "block"; 
}

function view_video( portfolio_id ) {
    document.querySelector(".main-page").style.display = "none";
    filter_elements(".videos .contenedor-video", "portfolio-id", portfolio_id); 
    document.querySelector(".videos").style.display = "block"; 
}

function filter_elements(selector, data, value) {
    console.log($(selector));
    $(selector).each(function( index ) {  
        if ($(this).data(data) == value) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
}

