<?php
class detalle{
	private $pdo;
    
    public $iddetalleproducto;
	 public $caracteristicas;
	 public $precio;
	 public $foto;
	 public $idproducto; 

	public function __CONSTRUCT()	{
		try{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Listar(){
		try{
			$result = array();

			$stm = $this->pdo->prepare(
			"SELECT idproducto,nombre,p.descripcion,precio,p.foto,p.foto2,fecharegistro,u.idusuario,nomusu,dni,pf.idportafolio,pf.descripcion as marca
from tbl_detalle p
inner join tbl_usuario u
on u.idusuario=p.idusuario
inner join tbl_portafolio pf
on pf.idportafolio=p.idportafolio");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function ListarCombouUsuarios(){
		try{
			$result = array();

			$stm = $this->pdo->prepare(
			"SELECT * from tbl_usuario");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function ListarComboPortafolio(){
		try{
			$result = array();

			$stm = $this->pdo->prepare(
			"SELECT * from tbl_portafolio");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Obtener($id){
		try {
			$stm = $this->pdo->prepare(
				"SELECT * from tbl_productos where  idproducto = ?");
			          

			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Eliminar($id){

		// seleccionamos la foto de la bd para elimnar
        $stmt_select = $this->pdo->prepare('SELECT foto FROM tbl_detalle WHERE idproducto =:prod_id');
        $stmt_select->execute(array(':prod_id'=>$id));
        $imgRow=$stmt_select->fetch(PDO::FETCH_ASSOC);
        unlink("producto-images/".$imgRow['foto']);


		try{
			$stm = $this->pdo->prepare("DELETE FROM tbl_detalle WHERE idproducto = ?");			          

			$stm->execute(array($id));

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Actualizar($data){
		try {
			$sql = "UPDATE tbl_detalle SET 
						nombre           = ?,
						descripcion      = ?, 
						precio           = ?,
						foto             = ?,
						fecharegistro    = ?,
						idusuario        = ?,
						idportafolio     = ?
						
						WHERE idproducto = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                        
                        $data->nombre,
						$data->descripcion,
						$data->precio,
                        $data->foto,
                        $data->fecharegistro,
                        $data->idusuario,
                        $data->idportafolio,

                        $data->idproducto,
					)
				);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Registrar(detalle $data)	{
		try {
		$sql = "INSERT into tbl_detalle(nombre,descripcion,precio,foto,fecharegistro,idusuario,idportafolio) values(?,?,?,?,?,?,?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
                    $data->nombre,
					$data->descripcion,
					$data->precio,
                    $data->foto,
                    $data->fecharegistro,
                    $data->idusuario,
                    $data->idportafolio
                    // $data->idpago                 
                    // date('Y-m-d')
                )
			);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}


}