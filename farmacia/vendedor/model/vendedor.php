<?php
class vendedor{
	private $pdo;

  public $idproducto;
	public $nombre;
	public $descripcion;
	public $precio;
	public $foto;
	public $fecharegistro;
	public $idusuario; 
	public $idportafolio; 

	public function __CONSTRUCT()	{
		try{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Listarslider(){
		try{
			$result = array();

			$stm = $this->pdo->prepare(
			"SELECT * from tbl_slider");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function ListarProductos(){
		try{
			$result = array();

			$stm = $this->pdo->prepare(
			"SELECT * from tbl_productos");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	} 
	
	public function Obtener($id){
		try {
			$stm = $this->pdo->prepare(
				"SELECT idproducto,nombre,p.foto as fotopro,fecharegistro,pf.idportafolio as idporta,pf.descripcion, pf.foto2 as banner
				from tbl_productos p
				inner join tbl_portafolio pf
				on pf.idportafolio=p.idportafolio  

				where p.idportafolio = ?");
			          

			$stm->execute(array($id)); 
			return $stm->fetch(PDO::FETCH_OBJ);

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Eliminar($id){


		// seleccionamos la foto de la bd para elimnar
        $stmt_select = $this->pdo->prepare('SELECT foto FROM tbl_vendedor WHERE idvendedor =:slid_id');
        $stmt_select->execute(array(':slid_id'=>$id));
        $imgRow=$stmt_select->fetch(PDO::FETCH_ASSOC);
        unlink("vendedor-images/".$imgRow['foto']);
		try{
			$stm = $this->pdo->prepare("DELETE FROM tbl_vendedor WHERE idvendedor = ?");			          

			$stm->execute(array($id));

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Actualizar($data){
		try {
			$sql = "UPDATE tbl_vendedor SET						
                        foto         = ?,
                        descripcion  = ?

				    WHERE idvendedor   = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                        $data->foto,
						$data->descripcion,

                        $data->idvendedor,
					)
				);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Registrar(vendedor $data)	{
		try {
		$sql = "INSERT into tbl_vendedor(foto,descripcion) values(?,?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
                    $data->foto,
                    $data->descripcion
                    // $data->idpago                 
                    // date('Y-m-d')
                )
			);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}


}