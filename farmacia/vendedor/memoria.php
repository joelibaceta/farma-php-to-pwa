<?php include("header.php"); ?>

<link rel="stylesheet" href="css/estilos.css"  type="text/css" />
<script src="js/jquery-2.0.3.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/funciones-min.js"></script>

<section class="ejemplo bg-memoria" style="background-image: url(img/fondo-memoria2.jpg);">
    <div class=controles>
        <!-- <div class=boton> -->
            <a id='iniciar' class="init boton controles-btn">Iniciar</a>
        <!-- </div> -->
        <div class=crono>
            0:<span>30</span>
        </div>
        <!-- <button class=boton> -->
            <a id='reiniciar' class="reinit boton controles-btn">Reiniciar</a>
        <!-- </button> -->
    </div>
    <div class="memoria">
        <article class="unoUno" data-pareja="uno">
        	<span><img src="img/champoo-pantene.png" class="img-responsive" alt=""></span>
        </article>        
        <article class="tresUno" data-pareja="tres">
            <span><img src="img/logohs.png" class="img-responsive" alt=""></span>
        </article>
        <article class="unoDos" data-pareja="uno">
            <span><img src="img/champoo-pantene.png" class="img-responsive" alt=""></span>
        </article>
        <article class="cuatroUno" data-pareja="cuatro">
            <span><img src="img/trecemea.png" class="img-responsive" alt=""></span>
        </article>        
        <article class="dosUno" data-pareja="dos">
            <span><img src="img/champoo-zedal.png" class="img-responsive" alt=""></span>
        </article>
        <article class="cuatroDos" data-pareja="cuatro">
            <span><img src="img/trecemea.png" class="img-responsive" alt=""></span>
        </article>
        <article class="tresDos" data-pareja="tres">
            <span><img src="img/logohs.png" class="img-responsive" alt=""></span>
        </article>        
        <article class="dosDos" data-pareja="dos">
            <span><img src="img/champoo-zedal.png" class="img-responsive" alt=""></span>
        </article>
    </div>
    <div class='retro msn'>
        <p>
        </p>
    </div>
    <div class="retro" id='final'>
        <p>
        </p>
    </div>
</section>
<?php include("footer.php"); ?>