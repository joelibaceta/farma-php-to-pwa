<?php include("header.php"); ?>
 
<script src="https://code.jquery.com/jquery.js"></script>
<section class="fondo-trivia" style="background-image: url(img/trivial-fondo2.jpg);">
    <div class="container">
        <div class="contenido-trivial">

            <div class="  text-center titulo-general">
                <h1>Difrute del juego y gane un premio</h1>
            </div>
            <div class="separador-linea"></div>
            <div class="formulariosss" >
            	<div id="pregunta"></div>
	            <div id="respuestas"></div>
	            <br>
				<div class="text-center">
					<input type="button" class="btn btn-comprobar " value="a ver si acerté" onclick="comprobar()">
				</div>
	            
            </div>
            <script>
                var preguntas = [
                    "<h3 class='titulos'>¿Quien descubrió América?</h3>",
                    "<h3 class='titulos'>¿Quién es el mayor superheroe de todos los tiempos?</h3>",
                    "<h3 class='titulos'>¿Por qué lo pájaros pían?</h3>"

                ];
                var respuestas = [
                    ["Cristobal Colón", "El Fari", "Yo mismo", "Tú"],
                    ["Spiderman", "Batman", "Yo mismo", "Tú mismo"],
                    ["Tienen hambre", "Tienen frío", "Están contentos", "Están tristes"]

                ];

                jugar();

                var indicie_respuesta_correcta;

                function jugar() {
                    var indice_aleatorio = Math.floor(Math.random() * preguntas.length);


                    var respuestas_posibles = respuestas[indice_aleatorio];


                    var posiciones = [0, 1, 2, 3];
                    var respuestas_reordenadas = [];

                    var ya_se_metio = false;
                    for (i in respuestas_posibles) {
                        var posicion_aleatoria = Math.floor(Math.random() * posiciones.length);
                        if (posicion_aleatoria == 0 && ya_se_metio == false) {
                            indicie_respuesta_correcta = i;
                            ya_se_metio = true;
                        }
                        respuestas_reordenadas[i] = respuestas_posibles[posiciones[posicion_aleatoria]];
                        posiciones.splice(posicion_aleatoria, 1);
                    }

                    var txt_respuestas = "";
                    for (i in respuestas_reordenadas) {
                        txt_respuestas += '<label class="radio-inline"><input type="radio" name="pp" value="' + i + '">' + respuestas_reordenadas[i] + '</label><br>';
                    }

                    document.getElementById("respuestas").innerHTML = txt_respuestas;
                    document.getElementById("pregunta").innerHTML = preguntas[indice_aleatorio];

                }
                function comprobar() {
                    var respuesta = $("input[type=radio]:checked").val();

                    //alert(respuesta);
                    //alert(indicie_respuesta_correcta);
                    if (respuesta == indicie_respuesta_correcta) {
                        // alert("que bien! Respuesta Correcta.");
                         sweetAlert("Que bien!", " Respuesta Correcta.", "success");
                    } else {
                        // alert("Ups..! Fallaste");
                         sweetAlert("Oops...", "Fallaste", "error");
                    }
                    jugar();
                }
            </script> 
        </div>
    </div>
</section>



<?php include("footer.php"); ?>