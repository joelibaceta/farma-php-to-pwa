<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li>usos</li>
            <li class="active">Agregar Nuevo usos</li>
        </ul>
        <!--breadcrumbs end -->
	    <h1 class="h1"></h1>
        <h1 > <strong><?php echo $usos->idusos != null ? "Actualizar usos" : 'Nuevo usos'; ?></strong> </h1>
	</div>
</div>
<style>
	.imagen-portafolio{
		max-width: 100%;
		height: 100%x;
		width: 100%;

	}
</style>

<div class="container">
<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Matenimiento de los usos</h3>
            <div class="actions pull-right">
                <i class="fa fa-chevron-down"></i>
                <i class="fa fa-times"></i>
            </div>
        </div>
        <div class="panel-body">          
            <div class="col-md-10 col-md-offset-1">
            <form action="?c=usos&a=Guardar" method="post" class="form-vertical" enctype="multipart/form-data">
            <!-- id usos --> <input type="hidden" name="txtcodUsos" id="txtcodUsos" class="form-control" value="<?php echo $usos->idusos; ?>">
           
                <!-- <div class="">
                    <div class="form-group">
                        <label for="txtDescripcion">RAZON SOCIAL DE LA MARCA</label>
                        <input name="txtDescripcion" id="txtDescripcion" class="form-control inputs" placeholder="ejem: enetperu SAC" value="<?php echo $usos->descripcion ; ?>">
                        
                    </div>
                </div> -->
                <div class="row form-group">
                    <div class="form-group">                       
                        <div class="col-md-8">
                           <?php if ($usos->idusos != null){ ?>
                                <label for="txtFotos">CONTENIDO</label>
                                <img src="usos-images/<?php echo $usos->fotousos; ?>" alt="" class="img-responsive  imagen-portafolio">
                                <input type="file"  name="txtFoto" id="txtFoto" accept="image/*" class="inputs">
                                <small>(maximo 1MB)</small>
                            <?php } else{ ?>
                                <label for="txtFotos">CONTENIDO</label>
                                <div id="imgPreview"></div>
                                <input type="file"  name="txtFoto" id="txtFoto" accept="image/*" class="inputs">
                                <small>(maximo 1MB)</small>
                            <?php }?> 
                        </div>                        
                    </div>
                </div>
                
                <div class="">
                    <div class="form-group">
                        <label for="cboPortafolio">PORTAFOLIO</label> 
                        <select name="cboPortafolio" id="cboPortafolio" class="form-control inputs">
                            <option value="">Seleciona un portafolio</option>
                            <?php foreach($this->model->ListarPortafolios() as $r): ?>                       
                                <option value="<?php echo $r->idportafolio; ?>"><?php echo $r->descripcion ; ?></option>
                            <?php endforeach; ?>

                            <?php echo $usos->idusos != null ? '  
                            <option value="'. $usos->idportafolio . '" selected="" >'. $usos->descripcion. '</option>
                           ' : '
                       
                            '; ?>
                            
                        </select>
                    </div>
                </div>
                <div class="text-center">
                    <div class="form-group">
                    <br><br>
                        <a href="usosindex.php" class="btn btn-default">Cancelar</a>
                        <input type="submit" class="btn btn-success" id="GuardarUsos" value="Guardar">
                    </div>
                </div>
            </form>    
            </div>
       </div>
    </div>
</div> 

</div>
<script src="js/validar-usos.js"></script>
<script>
	(function(){
		function filepreview(input){
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function(e){
					$('#imgPreview').html('<img src="'+e.target.result+'" class="img-responsive img-rounded" />')
				}
				reader.readAsDataURL(input.files[0]);
			}
		}

		$('#txtFoto').change(function(){
			filepreview(this);
		});
        
 
	})();
</script>