<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li>video</li>
            <li class="active">Agregar Nuevo video</li>
        </ul>
        <!--breadcrumbs end -->
	    <h1 class="h1"></h1>
        <h1 > <strong><?php echo $video->idvideo != null ? "Actualizar video" : 'Nuevo video'; ?></strong> </h1>
	</div>
</div>
<style>
	.imagen-portafolio{
		max-width: 100%;
		height: 100%x;
		width: 100%;

	}
</style>

<div class="container">
<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Matenimiento de los video</h3>
            <div class="actions pull-right">
                <i class="fa fa-chevron-down"></i>
                <i class="fa fa-times"></i>
            </div>
        </div>
        <div class="panel-body">          
            <div class="col-md-10 col-md-offset-1">
            <form action="?c=video&a=Guardar" method="post" class="form-vertical" enctype="multipart/form-data">
            <!-- id video --> <input type="hidden" name="txtcodvideo" id="txtcodvideo" class="form-control" value="<?php echo $video->idvideo; ?>">
           
                <div class="">
                    <div class="form-group">
                        <label for="txtTitulo">TITULO PARA EL VIDEO</label>
                        <input name="txtTitulo" id="txtTitulo" class="form-control inputs" placeholder="ejem: LIEMPIEZA RENOVADORA" value="<?php echo $video->titulo ; ?>">
                        
                    </div>
                </div>
                <div class="">
                <div class="form-group">                       
                    <!-- <div class="col-md-12"> -->
                    	<label for="txtFotos">URL DEL VIDEO</label>
                    	<input type="text" name="txtUrl" id="txtUrl" class="form-control inputs" placeholder="Ingrese la url del video" value="<?php echo $video->url ; ?>">
                        
                    <!-- </div>                         -->
                </div>
                </div>
                
                <div class="">
                    <div class="form-group">
                        <label for="cboPortafolio">PORTAFOLIO</label> 
                        <select name="cboPortafolio" id="cboPortafolio" class="form-control inputs">
                            <option value="">Seleciona un portafolio</option>
                            <?php foreach($this->model->ListarPortafolios() as $r): ?>                       
                                <option value="<?php echo $r->idportafolio; ?>"><?php echo $r->descripcion ; ?></option>
                            <?php endforeach; ?>

                            <?php echo $video->idvideo != null ? '  
                            <option value="'. $video->idportafolio . '" selected="" >'. $video->descripcion. '</option>
                           ' : '
                       
                            '; ?>
                            
                        </select>
                    </div>
                </div>
                <div class="text-center">
                    <div class="form-group">
                    <br><br>
                        <a href="videoindex.php" class="btn btn-default">Cancelar</a>
                        <input type="submit" class="btn btn-success" id="GuardarVideo" value="Guardar">
                    </div>
                </div>
            </form>    
            </div>
       </div>
    </div>
</div> 

</div>
<script src="js/validar-video.js"></script>
 