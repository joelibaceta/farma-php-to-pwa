 <div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li>detalle de los Producto</li>
            <li class="active">Todos los detalles de los Productos</li>
        </ul>
        <!--breadcrumbs end -->
	    <h1 class="h1">detalle de los Productos</h1>
	</div>
</div>
<style>
	.imagen-producto{
		max-width: 100%;
		height: 140px;

	}
</style>
<!-- <a class="btn btn-primary pull-right" href="?c=detalleProducto&a=Crud">Registrar Nuevo Producto</a> -->
<div class="text-right">
	<a href="productosindex.php" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Volver a Productos</a>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
          	<div class="panel-heading">
	            <h3 class="panel-title">Listado General de todos los detalles </h3>
	            <div class="actions pull-right">
	                <i class="fa fa-chevron-down"></i>
	                <i class="fa fa-times"></i>
	            </div>
          	</div>
          	<div class="panel-body">
          	<div class="col-md-12 swall">
	
			</div>          
	        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
	            <thead>
	                <tr>
	                    <th>#</th>
	                    <th >Imagen</th>
	                    <th>Nombre del Producto</th> 
	                    <th>caracteristicas</th>
	                    <th>Precio</th>	
	                    <th>Vendedor</th>	                    	                    
	                    <th >Acciones</th>
	                </tr>
	            </thead>
	     
	            <tbody>
	            <?php $cont=0; ?>
	            <?php foreach($this->model->Listar() as $r): ?>
	                <tr>
	                    <td><?php echo $cont=$cont +1; ?></td>
	                    <td align="center"><img src="detalle-producto-images/<?php echo $r->foto; ?>" class="img-responsive imagen-producto"></td>
	                    <td><?php echo $r->nombre_subproducto; ?></td> 
	                    <td><?php echo $r->caracteristicas; ?></td>
	                    <td><?php echo $r->precio; ?></td>
	                    <td><?php echo $r->nomusu .' '.$r->apeusu; ?></td>
	                    <td > 
			                <a  class="btn btn-xs btn-info" href="?c=productos&a=Crud3&id=<?php echo $r->iddetalleproducto; ?>">Editar</a>

			                <a  class="btn btn-xs btn-danger" style="color: #fff;" onclick="javascript:return confirm('���Seguro de eliminar este registro?');" href="?c=productos&a=Eliminar2&id=<?php echo $r->iddetalleproducto; ?>">Eliminar</a>
			            </td>	
	                </tr>
	            <?php endforeach; ?>   
	            </tbody>
	        </table>

           </div>
        </div>
        
    </div>
</div>