 <div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li>Productos</li>
            <li class="active">Todos los Productos</li>
        </ul>
        <!--breadcrumbs end -->
	    <h1 class="h1">Productos</h1>
	</div>
</div>
<style>
	.imagen-producto{
		max-width: 100%;
		height: 150px;

	}
</style>
<a class="btn btn-primary pull-right" href="?c=productos&a=Crud">Registrar Nuevo Producto</a>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
          	<div class="panel-heading">
	            <h3 class="panel-title">Listado General de todos los Productos</h3>
	            <div class="actions pull-right">
	                <i class="fa fa-chevron-down"></i>
	                <i class="fa fa-times"></i>
	            </div>
          	</div>
          	<div class="panel-body">
          	<div class="col-md-12 swall">
	
			</div>          
	        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
	            <thead>
	                <tr>
	                    <th>#</th>
	                    <th>Imagen</th>
	                    <th>banner</th>
	                    <th>Nombre del Producto</th>	                    
	                    <th>Fecha</th>
	                    <th>Marca / Portafolio</th>	                    	                    
	                    <th  >Acciones</th>
	                </tr>
	            </thead>
	     
	            <tbody>
	            <?php $cont=0; ?>
	            <?php foreach($this->model->Listar() as $r): ?>
	                <tr>
	                    <td><?php echo $cont=$cont +1; ?></td>
	                    <td><img src="producto-images/<?php echo $r->foto; ?>" class="img-responsive imagen-producto"></td>
	                    <td><img src="producto-images/<?php echo $r->foto2; ?>" class="img-responsive imagen-producto"></td>
	                    <td><?php echo $r->nombre; ?></td> 	                    
	                    <td><?php echo $r->fecharegistro; ?></td>
	                    <td><?php echo $r->marca; ?></td>
	                    <td >	                    	
			                <a  class="btn btn-xs btn-info" href="?c=productos&a=Crud&id=<?php echo $r->idproducto; ?>">Editar</a>
			                <a  class="btn btn-xs btn-danger" style="color: #fff;" onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" href="?c=productos&a=Eliminar&id=<?php echo $r->idproducto; ?>">Eliminar</a>
			                <a  class="btn btn-xs btn-success" href="?c=productos&a=Crud2&id=<?php echo $r->idproducto; ?>">Registrar Precios</a>
			            </td>	
	                </tr>
	            <?php endforeach; ?>   
	            </tbody>
	        </table>

           </div>
        </div>
    </div>
</div>