<div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li>Productos</li>
            <li class="active">Agregar Nuevo Producto</li>
        </ul>
        <!--breadcrumbs end -->
	    <h1 class="h1"></h1>
        <h1 > <strong><?php echo $pro->idproducto != null ? "Actualizar Producto" : 'Nuevo Producto'; ?></strong> </h1>
	</div>
</div>
<style>
    .imagen-slider, #imgPreview{
        max-width: 100%;
        height: 100%;
        width: 100%;
        margin-bottom: 5px;

    }
</style>

<div class="container">
<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Matenimiento de los Productos</h3>
            <div class="actions pull-right">
                <i class="fa fa-chevron-down"></i>
                <i class="fa fa-times"></i>
            </div>
        </div>
        <div class="panel-body">          
            <div class="col-md-10 col-md-offset-1">
            <form action="?c=productos&a=Guardar" method="post" class="form-vertical" enctype="multipart/form-data">
            <!-- id Producto --> <input type="hidden" name="txtCodProducto" id="txtCodProducto" class="form-control" value="<?php echo $pro->idproducto; ?>">
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="txtNombre">NOMBRE DEL PRODUCTO</label>
                        <input type="text" name="txtNombre" id="txtNombre" class="form-control inputs" value="<?php echo $pro->nombre; ?>" placeholder="ejem: Shampoo Pantene">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="txtColor">COLOR</label>
                        <input type="text" name="txtColor" id="txtColor" maxlength="7" class="form-control inputs" value="<?php echo $pro->color_nombre; ?>" placeholder=" #c2c2c2">
                    </div>
                </div>
                <div class="">
                    <div class="form-group">
                        <label for="txtSubTituloProducto">DESCRIPCIÓN DEL PRODUCTO</label>
                        <input type="text" name="txtSubTituloProducto" id="txtSubTituloProducto" class="form-control inputs" value="<?php echo $pro->subtitulo; ?>" placeholder="ejem: Cabello 100% libre de caspa">
                    </div>
                </div>
                <div class="">
                    <div class="form-group">
                        <label for="txtDescProducto">DESCRIPCIÓN DE BENEFICIO</label>
                        
                        <textarea name="txtDescProducto" id="txtDescProducto" class="form-control inputs" cols="30" rows="10" placeholder="ejem: esta version ofrese un cabello fresco limpio 100% libre de caspa"><?php echo $pro->descripcion; ?></textarea>
                        <small>(máximo: 500 caracteres)</small>
                    </div>
                </div>
                
                <div class="row form-group">
                    <div class="form-group">
                        <div class="col-md-8">
                            <label for="txtFotos">FOTO DEL PRODUCTO</label>
                            <?php if ($pro->idproducto != null){ ?>
                                <img src="producto-images/<?php echo $pro->fotopro; ?>" alt="" class="img-responsive  imagen-slider">
                                <input type="file"  name="txtFoto" id="txtFoto" accept="image/*" >
                                <small>(maximo 1MB)</small>
                            <?php } else{ ?>
                                <div id="imgPreview"></div>
                                <input type="file"  name="txtFoto" id="txtFoto" accept="image/*" class="inputs">
                                <small>(maximo 1MB)</small>

                            <?php }?>
                        </div>                         
                    </div>
                </div>

                <div class="row form-group">
                    <div class="form-group">
                        <div class="col-md-8">
                            <label for="txtFotos">BANNER DEL PRODUCTO</label>
                            <?php if ($pro->idproducto != null){ ?>
                                <img src="producto-images/<?php echo $pro->fotopro2; ?>" alt="" class="img-responsive  imagen-slider">
                                <input type="file"  name="txtFoto2" id="txtFoto2" accept="image/*" >
                                <small>(maximo 1MB)</small>
                            <?php } else{ ?>
                                <div id="imgPreview2"></div>
                                <input type="file"  name="txtFoto2" id="txtFoto2" accept="image/*" >
                                <small>(maximo 1MB)</small>

                            <?php }?>
                        </div>                         
                    </div>
                </div>
               
                
                <div class="">
                    <div class="form-group">
                        <label for="cboPortafolio">MARCA Y/O PORTAFOLIO</label> 
                        <select name="cboPortafolio" id="cboPortafolio" class="form-control inputs">
                            <option value="">Seleciona un Portafolio</option>
                            <?php foreach($this->model->ListarComboPortafolio() as $r): ?>                       
                                <option value="<?php echo $r->idportafolio; ?>"><?php echo $r->descripcion; ?></option>
                            <?php endforeach; ?>

                            <?php echo $pro->idproducto != null ? '  
                            <option value="'. $pro->idportafolio . '" selected="" >'. $pro->marca . '</option>
                           ' : '
                       
                            '; ?>
                            
                        </select>
                    </div>
                </div>
                <div class="text-center">
                    <div class="form-group">
                    <br><br>
                        <a href="productosindex.php" class="btn btn-default">Cancelar</a>
                        <input type="submit" class="btn btn-success" id="GuardarProductos" value="Guardar">
                    </div>
                </div>
            </form>    
            </div>
       </div>
    </div>
</div> 

</div>
<script src="js/validar-producto.js"></script>
<script>
    (function(){
        function filepreview(input){
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e){
                    $('#imgPreview').html('<img src="'+e.target.result+'" class="img-responsive img-rounded" />')
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#txtFoto').change(function(){
            filepreview(this);
        });

        // para la segunda foto
        function filepreview2(input){
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e){
                    $('#imgPreview2').html('<img src="'+e.target.result+'" class="img-responsive img-rounded" />')
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $('#txtFoto2').change(function(){
            filepreview2(this);
        });
    })();
</script>