 <div class="row">
    <div class="col-md-12">
        <!--breadcrumbs start -->
        <ul class="breadcrumb">
            <li><a href="#">Dashboard</a></li>
            <li>usuarios</li>
            <li class="active">Todos los Usuarios</li>
        </ul>
        <!--breadcrumbs end -->
	    <h1 class="h1">Usuarios</h1>
	</div>
</div>
<div class="">
	<a class="btn btn-primary pull-right" href="?c=usuario&a=Crud">Registrar Nuevo usuario</a>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
          	<div class="panel-heading">
	            <h3 class="panel-title">Listado General de todos los usuarios</h3>
	            <div class="actions pull-right">
	                <i class="fa fa-chevron-down"></i>
	                <i class="fa fa-times"></i>
	            </div>
          	</div>
          	<div class="panel-body">          
	            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
	            <thead>
	                <tr>
	                    <th>#</th>
	                    <th colspan="2">Nombres y Apellidos</th>
	                    <th>DNI</th>
	                    <th>Correo</th>
	                    <th>Contraseña</th>
	                    <th>Perfíl</th>
	                    <th colspan="2">Acciones</th>
	                </tr>
	            </thead>
	     
	            <tbody>
	            <?php $cont=0; ?>
	            <?php foreach($this->model->Listar() as $r): ?>
	                <tr>
	                    <td><?php echo $cont=$cont +1; ?></td>
	                    <td colspan="2"><?php echo $r->nomusu . ' ' . $r->apeusu; ?></td>
	                    <td><?php echo $r->dni; ?></td>
	                    <td><?php echo $r->corrusu; ?></td>
	                    <td><?php echo $r->clausu; ?></td>
	                    <td><?php echo $r->nomperfil; ?></td>
	                    <td colspan="2">
			                <a  class="btn btn-xs btn-info" href="?c=usuario&a=Crud&id=<?php echo $r->idusuario; ?>">Editar</a>

			                <a  class="btn btn-xs btn-danger" style="color: #fff;" onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" href="?c=usuario&a=Eliminar&id=<?php echo $r->idusuario; ?>">Eliminar</a>
			            </td>	
	                </tr>
	            <?php endforeach; ?>   
	            </tbody>
	            </table>

           </div>
        </div>
    </div>
</div>