var expr1 = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
var expr2 = /^[0-9]+$/;
$("#GuardarProductos").click(function() {
    var Nombre      = $("#txtNombre").val();
    var Color   = $("#txtColor").val();
    var SubTituloProducto = $("#txtSubTituloProducto").val();
    var DescProducto   = $("#txtDescProducto").val();
    var Foto    = $("#txtFoto").val();
    var Foto2   = $("#txtFoto2").val();
    var Portafolio   = $("#cboPortafolio").val();
    // validamos
    if (Nombre == "" &&  Color == ""&& SubTituloProducto == "" && DescProducto == ""  && Foto == "" && Foto2 == ""&& Portafolio == "") {
        $("input#txtNombre").focus();
        sweetAlert("Oops...", "Ingrese todos los Campos Requeridos", "warning");
        $(".inputs").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    } else if (Nombre == "" ) {
        $(".inputs").css({
            "border": "",
        });
        $("input#txtNombre").focus();
        sweetAlert("Oops...", "Digite el nombre del producto", "warning");
        $("#txtNombre").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    } else if (Color == "") {
        $(".inputs").css({
            "border": "",
        });
        $("input#txtColor").focus();
        sweetAlert("Oops...", "Digite el codigo del color ", "warning");
        $("#txtColor").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    }else if (SubTituloProducto == "") {
        $(".inputs").css({
            "border": "",
        });
        $("input#txtSubTituloProducto").focus();
        sweetAlert("Oops...", "Digite el subtitulo del producto", "warning");
        $("#txtSubTituloProducto").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    }  else if (DescProducto == "" ) {
        $(".inputs").css({
            "border": "",
        });
        $("input#txtDescProducto").focus();
        sweetAlert("Oops...", "Digite la descripciòn del producto", "warning");
        $("#txtDescProducto").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    } else if (Foto == ""  ) {
        $(".inputs").css({
            "border": "",
        });
        $("input#txtClave").focus();
        sweetAlert("Oops...", "Seleccione la foto del producto", "warning");
        $("#txtClave").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    } else if (Foto2 == ""  ) {
        $(".inputs").css({
            "border": "",
        });
        $("input#txtClave").focus();
        sweetAlert("Oops...", "Seleccione el banner del producto", "warning");
        $("#txtClave").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    } else if (Portafolio == "") {
        $(".inputs").css({
            "border": "",
        });
        $("select#cboPortafolio").focus();
        sweetAlert("Oops...", "Seleccione el portafolio para el producto", "warning");
        $("#cboPortafolio").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    }  else {
        form.submit();
    }
});