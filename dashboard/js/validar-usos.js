var expr1 = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
var expr2 = /^[0-9]+$/;
$("#GuardarUsos").click(function() { 
    var foto   = $("#txtFoto").val();
    var portafolio   = $("#cboPortafolio").val();
    // validamos
    if (foto == "" &&  portafolio == "") {
        $("input#txtFoto").focus();
        sweetAlert("Oops...", "Ingrese todos los Campos Requeridos", "warning");
        $(".inputs").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    }  else if (foto == "") {
        $(".inputs").css({
            "border": "",
        });
        $("input#txtFoto").focus();
        sweetAlert("Oops...", "Selecciona una imagen", "warning");
        $("#txtFoto").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    } else if (portafolio == "") {
        $(".inputs").css({
            "border": "",
        });
        $("select#cboPortafolio").focus();
        sweetAlert("Oops...", "Seleccione el portafolio para el video", "warning");
        $("#cboPortafolio").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    }  else {
        form.submit();
    }
});