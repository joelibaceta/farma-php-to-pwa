var expr1 = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
var expr2 = /^[0-9]+$/;
$("#GuardarSlider").click(function() {
    var descripcion      = $("#txtDescripcion").val();
    var foto   = $("#txtFoto").val();
    // validamos
    if (descripcion == "" && foto == "") {
        $("input#txtDescripcion").focus();
        sweetAlert("Oops...", "Ingrese todos los Campos Requeridos", "warning");
        $(".inputs").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    } else if (descripcion == "") {
        $(".inputs").css({
            "border": "",
        });
        $("input#txtDescripcion").focus();
        sweetAlert("Oops...", "Digite su dni \n Solo numeros", "warning");
        $("#txtDescripcion").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    } else if (foto == "") {
        $(".inputs").css({
            "border": "",
        });
        $("input#txtFoto").focus();
        sweetAlert("Oops...", "Selecciona una imagen para el slider", "warning");
        $("#txtFoto").css({
            "border": "solid 1px #a8272d",
        });
        return false;
    } else {
        form.submit();
    }
});