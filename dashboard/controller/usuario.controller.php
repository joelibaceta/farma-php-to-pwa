<?php
require_once 'model/usuario.php';

class usuarioController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new usuario();
    }
    
    public function Index(){
        require_once 'header.php';
        require_once 'view/usuario/listar-usuario.php';
        require_once 'footer.php';
    }
    
    public function Crud(){
        $usu = new usuario();
        
        if(isset($_REQUEST['id'])){
            $usu = $this->model->Obtener($_REQUEST['id']);
        }
        
        require_once 'header.php';
        require_once 'view/usuario/crud-usuario.php';
        require_once 'footer.php';
    }
    
    public function Guardar(){
        $usu = new usuario();
        
        $usu->idusuario =$_REQUEST['txtCodUsuario'];
        $usu->dni =$_REQUEST['txtDni'];
        $usu->nomusu =$_REQUEST['txtNombre'];
        $usu->apeusu =$_REQUEST['txtApellido'];
        $usu->corrusu =$_REQUEST['txtCorreo'];
        $usu->clausu =$_REQUEST['txtClave'];
        $usu->perfil_idperfil =$_REQUEST['cboPerfil'];
        

        $usu->idusuario > 0 
            ? $this->model->Actualizar($usu)
            : $this->model->Registrar($usu);
        
        header('Location: usuarioindex.php');
    }
    
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        header('Location: usuarioindex.php');
    }

}