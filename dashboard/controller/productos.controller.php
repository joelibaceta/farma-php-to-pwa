<?php
require_once 'model/productos.php';

class productosController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new productos();
    }
    
    public function Index(){
        require_once 'header.php';
        require_once 'view/productos/listar-productos.php';
        require_once 'footer.php';
    }
      // funcion para agregar el detalle de los productos
     public function Crud2(){
        $pro = new productos();
        
        if(isset($_REQUEST['id'])){
            $pro = $this->model->Obtener2($_REQUEST['id']);
        }
        
        require_once 'header.php';
        require_once 'view/detalleProducto/agregar-detalleProducto.php';
        require_once 'footer.php';
    }
    // funcion para editar el detalle de los productos
     public function Crud3(){
        $pro = new productos();
        
        if(isset($_REQUEST['id'])){
            $pro = $this->model->Obtener3($_REQUEST['id']);
        }
        
        require_once 'header.php';
        require_once 'view/detalleProducto/editar-detalleProducto.php';
        require_once 'footer.php';
    }
    public function Crud(){
        $pro = new productos();
        
        if(isset($_REQUEST['id'])){
            $pro = $this->model->Obtener($_REQUEST['id']);
        }
        
        require_once 'header.php';
        require_once 'view/productos/crud-productos.php';
        require_once 'footer.php';
    }
    
          // para agregar y editar los sub productos
    public function Guardar2(){
        $pro = new productos();

        $imgFile    = $_FILES['txtFoto']['name'];
        $tmp_dir    = $_FILES['txtFoto']['tmp_name'];
        $imgSize    = $_FILES['txtFoto']['size'];
        $upload_dir = 'detalle-producto-images/'; // upload directory
    
        $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension    
        // valid image extensions
        $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions    
        // rename uploading image
        // $userpic = rand(1000,1000000).".".$imgExt;
        $userpic = rand(1000,1000000).".".$imgExt;
        // allow valid image file formats
        if(in_array($imgExt, $valid_extensions)){           
            // Check file size '1MB'
            if($imgSize < 1000000)              {
                move_uploaded_file($tmp_dir,$upload_dir.$userpic);
            }
            else{
                header("Location: detalleProductoindex.php");
            }
        }
        else{
            header("Location: detalleProductoindex.php");     
        }
        // SCRIPT PARA CARGAR IMAGEN DE PRECIO
        $imgFile2    = $_FILES['txtFoto2']['name'];
        $tmp_dir2    = $_FILES['txtFoto2']['tmp_name'];
        $imgSize2    = $_FILES['txtFoto2']['size'];
        $upload_dir2 = 'detalle-producto-images/'; // upload directory
    
        $imgExt2 = strtolower(pathinfo($imgFile2,PATHINFO_EXTENSION)); // get image extension    
        // valid image extensions
        $valid_extensions2 = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions    
        // renombramos el nombre la foto
        // $userpic2 = rand(1000,1000000).".".$imgExt2;
        $userpic2 = rand(1000,1000000).".".$imgExt2; 
        // allow valid image file formats
        if(in_array($imgExt2, $valid_extensions2)){           
            // Check file size '1MB'
            if($imgSize2 < 1000000){
                move_uploaded_file($tmp_dir2,$upload_dir2.$userpic2);
            }
            else{
                header("Location: detalleProductoindex.php");
            }
        }
        else{
            header("Location: detalleProductoindex.php");     
        } 

        $pro->nombre_subproducto   =$_REQUEST['txtNomProducto'];
        $pro->caracteristicas    =$_REQUEST['txtDescripcion'];
        $pro->precio   =$_REQUEST['txtPrecio'];
        $pro->foto        =$userpic;
        $pro->foto2        =$userpic2;
        $pro->idproducto =$_REQUEST['txtCodProducto'];        
        $pro->idusuario =$_REQUEST['cboUsuario'];   

        $pro->iddetalleproducto > 0 
            ? $this->model->Actualizarsub($pro)
            : $this->model->Registrarsub($pro);
        
        header('Location: detalleProductoindex.php');
    }
       // para   editar los sub productos
    public function Guardar3(){
        $pro = new productos();

        $imgFile    = $_FILES['txtFoto']['name'];
        $tmp_dir    = $_FILES['txtFoto']['tmp_name'];
        $imgSize    = $_FILES['txtFoto']['size'];
        $upload_dir = 'detalle-producto-images/'; // upload directory
    
        $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension    
        // valid image extensions
        $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions    
        // rename uploading image
        // $userpic = rand(1000,1000000).".".$imgExt;
        $userpic = rand(1000,1000000).".".$imgExt;
        // allow valid image file formats
        if(in_array($imgExt, $valid_extensions)){           
            // Check file size '1MB'
            if($imgSize < 1000000)              {
                move_uploaded_file($tmp_dir,$upload_dir.$userpic);
            }
            else{
                header("Location: detalleProductoindex.php");
            }
        }
        else{
            header("Location: detalleProductoindex.php");     
        }
        // SCRIPT PARA CARGAR IMAGEN DE PRECIO
        $imgFile2    = $_FILES['txtFoto2']['name'];
        $tmp_dir2    = $_FILES['txtFoto2']['tmp_name'];
        $imgSize2    = $_FILES['txtFoto2']['size'];
        $upload_dir2 = 'detalle-producto-images/'; // upload directory
    
        $imgExt2 = strtolower(pathinfo($imgFile2,PATHINFO_EXTENSION)); // get image extension    
        // valid image extensions
        $valid_extensions2 = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions    
        // renombramos el nombre la foto
        // $userpic2 = rand(1000,1000000).".".$imgExt2;
        $userpic2 = rand(1000,1000000).".".$imgExt2; 
        // allow valid image file formats
        if(in_array($imgExt2, $valid_extensions2)){           
            // Check file size '1MB'
            if($imgSize2 < 1000000){
                move_uploaded_file($tmp_dir2,$upload_dir2.$userpic2);
            }
            else{
                header("Location: detalleProductoindex.php");
            }
        }
        else{
            header("Location: detalleProductoindex.php");     
        }
         $pro->iddetalleproducto  =$_REQUEST['txtCodProducto'];
        $pro->nombre_subproducto   =$_REQUEST['txtNomProducto'];
        $pro->caracteristicas    =$_REQUEST['txtDescripcion'];
        $pro->precio   =$_REQUEST['txtPrecio'];
        $pro->foto        =$userpic;
        $pro->foto2        =$userpic2;
        $pro->idproducto =$_REQUEST['txtCodProducto'];        
        $pro->idusuario =$_REQUEST['cboUsuario'];   

        $pro->iddetalleproducto > 0 
            ? $this->model->Actualizarsub($pro)
            : $this->model->Registrar($pro);
        
        header('Location: detalleProductoindex.php');
    }

  
    
    
    public function Guardar(){
        $pro = new productos();

        $imgFile    = $_FILES['txtFoto']['name'];
        $tmp_dir    = $_FILES['txtFoto']['tmp_name'];
        $imgSize    = $_FILES['txtFoto']['size'];
        $upload_dir = 'producto-images/'; // upload directory
    
        $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension    
        // valid image extensions
        $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions    
        // rename uploading image
        // $userpic = rand(1000,1000000).".".$imgExt;
        $userpic = rand(1000,1000000).".".$imgExt;
        // allow valid image file formats
        if(in_array($imgExt, $valid_extensions)){           
            // Check file size '1MB'
            if($imgSize < 1000000)              {
                move_uploaded_file($tmp_dir,$upload_dir.$userpic);
            }
            else{
                header("Location: productosindex.php");
            }
        }
        else{
            header("Location: productosindex.php");     
        }
        // --------------------------------------------------------------------
        // insertando la foto numero 2 banner del producto
        // --------------------------------------------------------------------
        $imgFile2    = $_FILES['txtFoto2']['name'];
        $tmp_dir2    = $_FILES['txtFoto2']['tmp_name'];
        $imgSize2    = $_FILES['txtFoto2']['size'];
        $upload_dir2 = 'producto-images/'; // upload directory
    
        $imgExt2 = strtolower(pathinfo($imgFile2,PATHINFO_EXTENSION)); // get image extension    
        // valid image extensions
        $valid_extensions2 = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions    
        // rename uploading image
        // $userpic = rand(1000,1000000).".".$imgExt2;
        $userpic2 = rand(1000,1000000).".".$imgExt2;
        // allow valid image file formats
        if(in_array($imgExt2, $valid_extensions2)){           
            // Check file size '1MB'
            if($imgSize2 < 1000000)              {
                move_uploaded_file($tmp_dir2,$upload_dir2.$userpic2);
            }
            else{
                header("Location: productosindex.php");
            }
        }
        else{
            header("Location: productosindex.php");     
        }


        $pro->idproducto    =$_REQUEST['txtCodProducto'];
        $pro->nombre        =$_REQUEST['txtNombre'];
        $pro->color_nombre  =$_REQUEST['txtColor'];
        $pro->subtitulo     =$_REQUEST['txtSubTituloProducto'];
        $pro->descripcion   =$_REQUEST['txtDescProducto'];
        $pro->precio        =$_REQUEST['txtPrecio'];
        $pro->foto          =$userpic;
        $pro->foto2         =$userpic2;
        $pro->fecharegistro =date('Y-m-d');
        // $pro->idusuario  =$_REQUEST['cboUsuario'];
        $pro->idportafolio  =$_REQUEST['cboPortafolio'];
        

        $pro->idproducto > 0 
            ? $this->model->Actualizar($pro)
            : $this->model->Registrar($pro);
        
        header('Location: productosindex.php');
    }
    
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['id']);
        header('Location: productosindex.php');
    }
       public function Eliminar2(){
        $this->model->Eliminar2($_REQUEST['id']);
        header('Location: detalleProductoindex.php');
    }


}