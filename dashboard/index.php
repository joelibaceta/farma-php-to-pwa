<?php include('header.php'); ?>
        <?php
            // $id_usuario = $_SESSION['full_user'];
            $query = "SELECT  COUNT(*) as  totalusuarios from tbl_usuario";

            //echo $query;
            $result = mysqli_query($con, $query); 
            $i = 1;            
            if (mysqli_affected_rows($con) != 0) {
                while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                    $totalusurios = $row['totalusuarios']; 
                }
            } 
            $i = 1;
            ?>

            <?php 
            //  funcion para productos
            // $id_usuario = $_SESSION['full_user'];
            $query2 = "SELECT  COUNT(*) as  totprod from tbl_productos";
            //echo $query2;
            $result2 = mysqli_query($con, $query2); 
            $i = 2;

            if (mysqli_affected_rows($con) != 0) {
                while ($row2 = mysqli_fetch_array($result2, MYSQLI_ASSOC)) {
                    $total_productos = $row2['totprod']; 
                }
            }
            $i = 2;
            ?>

            <?php 
            //  funcion para productos
            // $id_usuario = $_SESSION['full_user'];
            $query3 = "SELECT  COUNT(*) as  totslider from tbl_slider";
            //echo $query3;
            $result3 = mysqli_query($con, $query3); 
            $i = 2;

            if (mysqli_affected_rows($con) != 0) {
                while ($row3 = mysqli_fetch_array($result3, MYSQLI_ASSOC)) {
                    $total_sliders = $row3['totslider']; 
                }
            }
            $i = 2;
            ?>
             <?php 
            //  funcion para productos
            // $id_usuario = $_SESSION['full_user'];
            $query3 = "SELECT  COUNT(*) as  totalportafolio from tbl_portafolio";
            //echo $query3;
            $result3 = mysqli_query($con, $query3); 
            $i = 2;

            if (mysqli_affected_rows($con) != 0) {
                while ($row3 = mysqli_fetch_array($result3, MYSQLI_ASSOC)) {
                    $total_portafolio = $row3['totalportafolio']; 
                }
            }
            $i = 2;
            ?>
        <!--tiles start-->
        <div class="row">

            <a href="usuarioindex.php" class="col-md-3 col-sm-6">
                <div class="dashboard-tile detail tile-red">
                        
                    <div class="content">
                        <h1 class="text-left timer" data-from="0" data-to="<?php echo $totalusurios; ?>" data-speed="2500"> </h1>
                        <p>Usuarios</p>
                    </div>
                    <div class="icon"><i class="fa fa-users"></i>
                    </div>
                </div>
            </a>
            <a href="productosindex.php" class="col-md-3 col-sm-6">
                <div class="dashboard-tile detail tile-turquoise">
                    <div class="content">
                        <h1 class="text-left timer" data-from="0" data-to="<?php echo $total_productos; ?>" data-speed="2500"> </h1>
                        <p>Productos</p>
                    </div>
                    <div class="icon"><i class="fa fa-bar-chart-o" aria-hidden="true"></i>
                    </div>
                </div>
            </a>
            
            <a href="portafolioindex.php" class="col-md-3 col-sm-6">
                <div class="dashboard-tile detail tile-blue">
                    <div class="content">
                        <h1 class="text-left timer" data-from="0" data-to="<?php echo $total_portafolio; ?>" data-speed="2500"> </h1>
                        <p>Marca / Portafolio</p>
                    </div>
                    <div class="icon"><i class="fa fa fa-briefcase"></i>
                    </div>
                </div>
            </a>
            <a href="sliderindex.php" class="col-md-3 col-sm-6">
                <div class="dashboard-tile detail tile-purple">
                    <div class="content">
                        <h1 class="text-left timer" data-to="<?php echo $total_sliders; ?>" data-speed="2500"> </h1>
                        <p>Sliders</p>
                    </div>
                    <div class="icon"><i class="fa fa-picture-o"></i>
                    </div>
                </div>
            </a>
        </div>
        <!--tiles end-->
        <!--dashboard charts and map start-->
        <!-- <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Sales for 2014</h3>
                        <div class="actions pull-right">
                            <i class="fa fa-chevron-down"></i>
                            <i class="fa fa-times"></i>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div id="sales-chart" style="height: 250px;"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Office locations</h3>
                        <div class="actions pull-right">
                            <i class="fa fa-chevron-down"></i>
                            <i class="fa fa-times"></i>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="map" id="map" style="height: 250px;"></div>
                    </div>
                </div>
            </div>
        </div> -->
        <!--dashboard charts and map end-->
        <!--ToDo start-->

       <!--  <div class="row">
            
            <div class="col-md-4">
                <div class="panel panel-solid-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Estado del Clima</h3>
                        <div class="actions pull-right">
                            <i class="fa fa-chevron-down"></i>
                            <i class="fa fa-times"></i>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="text-center small-thin uppercase">Día </h3>
                                <div class="text-center">
                                    <canvas id="clear-day" width="110" height="110"></canvas>
                                    <h4>62°C</h4>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h3 class="text-center small-thin uppercase">Noche</h3>
                                <div class="text-center">
                                    <canvas id="partly-cloudy-night" width="110" height="110"></canvas>
                                    <h4>44°C</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col-md-2">
                                <h6 class="text-center small-thin uppercase">Lun</h6>
                                <div class="text-center">
                                    <canvas id="partly-cloudy-day" width="32" height="32"></canvas>
                                    <span>48°C</span>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <h6 class="text-center small-thin uppercase">Mar</h6>
                                <div class="text-center">
                                    <canvas id="rain" width="32" height="32"></canvas>
                                    <span>39°C</span>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <h6 class="text-center small-thin uppercase">Mie</h6>
                                <div class="text-center">
                                    <canvas id="sleet" width="32" height="32"></canvas>
                                    <span>32°C</span>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <h6 class="text-center small-thin uppercase">Jue</h6>
                                <div class="text-center">
                                    <canvas id="snow" width="32" height="32"></canvas>
                                    <span>28°C</span>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <h6 class="text-center small-thin uppercase">Vie</h6>
                                <div class="text-center">
                                    <canvas id="wind" width="32" height="32"></canvas>
                                    <span>40°C</span>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <h6 class="text-center small-thin uppercase">Sab</h6>
                                <div class="text-center">
                                    <canvas id="fog" width="32" height="32"></canvas>
                                    <span>42°C</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>Navegadores</h4>
                        <div id="donut-example"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">                
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Estado del sistema</h3>
                                <div class="actions pull-right">
                                    <i class="fa fa-chevron-down"></i>
                                    <i class="fa fa-times"></i>
                                </div>
                            </div>
                            <div class="panel-body">

                          <span class="sublabel">Usuarios activos</span>
                          <div class="progress progress-striped">
                            <div class="progress-bar progress-bar-info" style="width: <?php echo $total_productos*0.01."%"?>"><?php echo $total_productos*0.01; ?></div>
                          </div>

                          <span class="sublabel"> Productos</span>
                          <div class="progress progress-striped">
                           <div class="progress-bar progress-bar-default" style="width: 60%">60%</div>
                          </div>

                          <span class="sublabel">Sliders </span>
                          <div class="progress progress-striped">
                            <div class="progress-bar progress-bar-primary" style="width: 80%">80%</div>
                          </div>
          
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div> -->

        <!--ToDo end-->
        


<?php include('footer.php'); ?>