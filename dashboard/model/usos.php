<?php
class usos{
	private $pdo;

  public $idusos;
	public $foto; 
	public $portafolio_idportafolio; 

	public function __CONSTRUCT()	{
		try{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	 public function Listar(){
		try{
			$result = array();

			$stm = $this->pdo->prepare(
			"SELECT po.idportafolio, descripcion,idusos, u.foto as fotousos
from tbl_portafolio po
inner join tbl_usos u
on u.portafolio_idportafolio=po.idportafolio");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	} 
	public function ListarPortafolios(){
		try{
			$result = array();

			$stm = $this->pdo->prepare(
			"SELECT * from tbl_portafolio");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	} 
	
	public function Obtener($id){
		try {
			$stm = $this->pdo->prepare(
				"SELECT po.idportafolio, descripcion,idusos, u.foto as fotousos
from tbl_portafolio po
inner join tbl_usos u
on u.portafolio_idportafolio=po.idportafolio

				where idusos = ?");
			          

			$stm->execute(array($id)); 
			return $stm->fetch(PDO::FETCH_OBJ);

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Eliminar($id){


		// seleccionamos la foto de la bd para elimnar
        $stmt_select = $this->pdo->prepare('SELECT foto FROM tbl_usos WHERE idusos =:slid_id');
        $stmt_select->execute(array(':slid_id'=>$id));
        $imgRow=$stmt_select->fetch(PDO::FETCH_ASSOC);
        unlink("usos-images/".$imgRow['foto']);
		try{
			$stm = $this->pdo->prepare("DELETE FROM tbl_usos WHERE idusos = ?");			          

			$stm->execute(array($id));

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Actualizar($data){
		try {
			$sql = "UPDATE tbl_usos SET						
                        foto         = ?,
                        portafolio_idportafolio  = ?

				    WHERE idusos   = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                        $data->foto,
						$data->portafolio_idportafolio,

                        $data->idusos,
					)
				);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Registrar(usos $data)	{
		try {
		$sql = "INSERT into tbl_usos(foto,portafolio_idportafolio) values(?,?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
                    $data->foto,
                    $data->portafolio_idportafolio
                    // $data->idpago                 
                    // date('Y-m-d')
                )
			);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}


}