<?php
class slider{
	private $pdo;
    public $idsliderfalso;
    public $idslider;
	public $foto;
	public $descripcion;

	public function __CONSTRUCT()	{
		try{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}

	public function Listar(){
		try{
			$result = array();

			$stm = $this->pdo->prepare(
			"SELECT * from tbl_slider");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function ListarRegistros(){
		try{
			$result = array();

			$stm = $this->pdo->prepare(
			"SELECT count(*) as totalSlider from tbl_slider");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	} 
	public function Obtener($id){
		try {
			$stm = $this->pdo->prepare(
				"SELECT * from tbl_slider where idslider = ?");
			          

			$stm->execute(array($id));
			return $stm->fetch(PDO::FETCH_OBJ);

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Eliminar($id){


		// seleccionamos la foto de la bd para elimnar
        $stmt_select = $this->pdo->prepare('SELECT foto FROM tbl_slider WHERE idslider =:slid_id');
        $stmt_select->execute(array(':slid_id'=>$id));
        $imgRow=$stmt_select->fetch(PDO::FETCH_ASSOC);
        unlink("slider-images/".$imgRow['foto']);
		try{
			$stm = $this->pdo->prepare("DELETE FROM tbl_slider WHERE idslider = ?");			          

			$stm->execute(array($id));

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Actualizar($data){
		try {
			$sql = "UPDATE tbl_slider SET						
                        foto         = ?,
                        descripcion  = ?

				    WHERE idslider   = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                        $data->foto,
						$data->descripcion,

                        $data->idslider,
					)
				);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Registrar(slider $data)	{
		try {
		$sql = "INSERT into tbl_slider(foto,descripcion) values(?,?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
                    $data->foto,
                    $data->descripcion
                    // $data->idpago                 
                    // date('Y-m-d')
                )
			);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}


}